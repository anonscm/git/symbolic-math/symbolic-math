package org.evolvis.math.symbolic.maven;

import java.io.File;
import java.io.IOException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.evolvis.math.symbolic.environment.ClassProcessor;

/**
 * 
 * 
 * @goal algorator
 */
public class AlgoratorMojo extends AbstractMojo {

	private static final String TEMPLATE_SUFFIX = ".java.alg";
	private static final String CLASSFILE_SUFFIX = ".java";

	private final Log logger = getLog();
	
    /**
     * An Array wich holds java source code files which should be checked for
     * algorator comments to generate java code in this files.
     *
     * @parameter
     */
    private File[] files;
    
    public void execute() throws MojoExecutionException {
    	int count = files == null ? 0 : files.length;
    	int processed = 0;
    	for (int i = 0; i < count; i ++) {
    		File file = files[i];
    		if (file != null) {
    			if (!file.exists())
    				logger.error("file \""+file.getAbsolutePath()+"\" does not exist");
    			else if (!file.isFile())
    				logger.error("\""+file.getAbsolutePath()+"\" is not a file");
    			else if (!file.getName().endsWith(TEMPLATE_SUFFIX))
    				logger.error("file \""+file.getAbsolutePath()+"\" does not has the suffix \""+TEMPLATE_SUFFIX+"\"");
    			else {
    				String classFileName = file.getAbsolutePath();
    				classFileName = classFileName.substring(0, classFileName.length()-TEMPLATE_SUFFIX.length());
    				classFileName += CLASSFILE_SUFFIX;
    				try {
						if (ClassProcessor.creatFunctions(file, new File(classFileName), true))
							processed ++;
					} catch (IOException e) {
	    				logger.error("error reading file \""+file.getAbsolutePath()+"\"", e);
					}
    			}
    		}
    	}
    	if (count == 0)
    		logger.info("Nothing to process");
    	else if (processed == 0)
    		logger.info("Nothing to process - all files are up to date");
    	else
    		logger.info("Processed "+processed+" file" + (processed == 1 ? "" : "s"));
    }
}
