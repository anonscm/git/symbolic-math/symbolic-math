package org.evolvis.math.symbolic;

public final class Helper {
	
	private Helper() {}

    public static final double[] copy(double[] buffer, double[] x) {
    	int n = x.length;
    	if (buffer == null)
    		buffer = new double[n];
    	else if (n != buffer.length)
    		throw new RuntimeException("matrices must have the same dimension");
    	for (int i = 0; i < n; i ++)
    		buffer[i] = x[i];
    	return buffer;
    }

    public static final double[][] copy(double[][] buffer, double[][] A) {
    	int height = A.length;
    	int width = A[0].length;
    	if (buffer == null)
    		buffer = new double[height][width];
    	else if (height != buffer.length || width != buffer[0].length)
    		throw new RuntimeException("matrices must have the same dimension");
    	for (int i = 0; i < height; i ++)
        	for (int j = 0; j < width; j ++)
    			buffer[i][j] = A[i][j];
    	return buffer;
    }
    
    public static final void print(double[][] A) {
    	int height = A.length;
    	int width = A[0].length;
    	for (int i = 0; i < height; i ++) {
    		System.out.print("|");
        	for (int j = 0; j < width; j ++) {
        		if (j > 0)
        			System.out.print(", ");
        		System.out.print(A[i][j]);
        	}
    		System.out.println("|");
    	}
    }

    //TODO: avoid creation of new object
    public static final void print(double[] x) {
    	print(new double[][] {x});
    }
    
    public static final double maxNorm(double[] x, double[][] Sx) {
		int n = x.length;
		if (n != Sx.length || n != Sx[0].length)
			throw new RuntimeException("matrices dimension does not fit");
		double max = Double.MIN_VALUE;
		for (int i = 0; i < n; i ++) {
			double t = x[i] * x[i] / Sx[i][i];
			if (t > max)
				max = t;
		}
		return Math.sqrt(max);
	}
    
    public static final double maxNorm(double x, double Sx) {
		return Math.sqrt(x * x / Sx);
	}
    
    public static final boolean isValid(double[][] A) {
    	int height = A.length;
    	for (int i = 0; i < height; i ++)
    		if (! isValid(A[i]))
    			return false;
    	return true;
    }
    
    public static final boolean isValid(double[] x) {
    	int height = x.length;
    	for (int i = 0; i < height; i ++)
    		if (Double.isNaN(x[i]) || Double.isInfinite(x[i]))
    			return false;
    	return true;
    }
}
