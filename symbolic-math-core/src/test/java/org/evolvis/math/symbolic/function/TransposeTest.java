package org.evolvis.math.symbolic.function;

import junit.framework.TestCase;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.operator.Transpose;

public class TransposeTest extends TestCase {

	public void testApply() {
		Matrix m1 = Matrix.create("(1,2,3;4,5,6)");
		Matrix m2 = Matrix.create("(1,4;2,5;3,6)");
		assertEquals(m2, Transpose.apply(m1));
	}

}
