package org.evolvis.math.symbolic;

import java.math.BigInteger;

import junit.framework.TestCase;

public class NumberTest extends TestCase {

	private static final int RETRY_COUNT = 1000;

	public void testGetDenominator() throws OperandInitializationException {
		for (int i=0; i<RETRY_COUNT; i++) {
			Number n = createRandomNumber(true);
			assertTrue("denominator must be greater to zero", n.getDenominator().compareTo(BigInteger.ZERO) > 0);
		}
	}
	
	public void testConstants() {
		assertEquals(Number.TEN, Number.TEN.multiply(Number.ONE));
		assertEquals(Number.TEN.multiply(Number.TEN_INV), Number.ONE);
		assertEquals(Number.ONE.multiply(Number.ONE_NEG), Number.ONE_NEG);
		assertEquals(Number.ONE.add(Number.ONE_NEG), Number.ZERO);
		assertEquals(Number.ZERO, Number.TEN.multiply(Number.ZERO));
	}

	public void testRepresenations() throws OperandInitializationException {
		Number n;
		n = Number.create("-12.3456");
		assertEquals(n, Number.create("123456/-10000"));
		assertEquals(n, Number.create("-123456/10000"));
		n = Number.create("0.0012");
		assertEquals(n, Number.create("-24/-20000"));
	}

	public void testInvert() throws OperandInitializationException {
		for (int i=0; i<RETRY_COUNT; i++) {
			Number n = createRandomNumber(false);
			assertEquals(n.invert().multiply(n), Number.ONE);
		}
	}

	public void testSubtract() throws OperandInitializationException {
		assertEquals(Number.create("11"), Number.create("77").subtract(Number.create("66")));
		assertEquals(Number.create("-12"), Number.create("12").subtract(Number.create("24")));
	}

	public void testDivide() throws OperandInitializationException {
		assertEquals(Number.create("11"), Number.create("11").divide(Number.create("1")));
		assertEquals(Number.create("-3"), Number.create("12").divide(Number.create("-4")));
		assertEquals(Number.create("-3"), Number.create("-12").divide(Number.create("4")));
		assertEquals(Number.create("5"), Number.create("30").divide(Number.create("6")));
	}

	public void testToString() throws OperandInitializationException {
		for (int i=0; i<RETRY_COUNT; i++) {
			Number n = createRandomNumber(true);
			assertEquals(n, Number.create(n.toString()));
		}
	}

	public void testDivideByZero() throws OperandInitializationException {
		for (int i=0; i<RETRY_COUNT; i++) {
			Number n = Number.create(createRandomNumberString(true, true)+"/0");
			assertEquals(Number.NAN, n);
		}
	}

	public void testEqualsAndHashCode() throws OperandInitializationException {
		Number n1 = Number.create("-12/77");
		Number n2 = Number.create("24/-154"); // same as n1
		Number n3 = Number.create("-13/77");  // different
		// does the equals method work correctly?
		assertEquals(n1, n1);
		assertEquals(n2, n2);
		assertEquals(n1, n2);
		assertEquals(n2, n1);
		assertFalse(n1.equals(n3));
		assertFalse(n2.equals(n3));
		assertFalse(n3.equals(n1));
		assertFalse(n3.equals(n2));
		// does the hashCode method work correctly?
		assertEquals(n1.hashCode(), n1.hashCode());
		assertEquals(n2.hashCode(), n2.hashCode());
		assertEquals(n3.hashCode(), n3.hashCode());
		assertEquals(n1.hashCode(), n2.hashCode());
	}
	
	// helper methods

	private char createRandomNumberChar() {
		return (char) ('0'+Math.round(Math.random()*9));
	}
	
	private String createRandomNumberString(boolean sign, boolean zero) {
		StringBuilder sb = new StringBuilder();
		if (sign && Math.random() < 0.5)
			sb.append('-');
		do
			sb.append(createRandomNumberChar());
		while (Math.random() < 0.95);
		String ret = sb.toString();
		if (!zero && BigInteger.ZERO.equals(new BigInteger(ret)))
			return createRandomNumberString(sign, zero); // try again
		return ret;
	}

	private Number createRandomNumber(boolean zero) throws OperandInitializationException {
		StringBuilder sb = new StringBuilder(createRandomNumberString(true, true));
		Number number;
		if (Math.random() < 0.333333)
			number = Number.create(sb.append(OperandSerializer.CHAR_NUMBER_SEPARATOR_FRACTION).append(createRandomNumberString(true, false)).toString());
		else if (Math.random() < 0.333333)
			number = Number.create(sb.append(OperandSerializer.CHAR_NUMBER_SEPARATOR_DECIMAL).append(createRandomNumberString(false, true)).toString());
		number = Number.create(sb.toString());
		return zero || !Number.ZERO.equals(number) ? number : createRandomNumber(zero);
	}
}
