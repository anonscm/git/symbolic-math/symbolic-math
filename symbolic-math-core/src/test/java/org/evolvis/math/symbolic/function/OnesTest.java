package org.evolvis.math.symbolic.function;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.OperandInitializationException;
import org.evolvis.math.symbolic.operator.Ones;

import junit.framework.TestCase;

public class OnesTest extends TestCase {

	public void testApply() throws OperandInitializationException {
		assertEquals(Ones.apply(4),
				Matrix.create("(1,0,0,0; 0,1,0,0; 0,0,1,0; 0,0,0,1)"));
	}
}
