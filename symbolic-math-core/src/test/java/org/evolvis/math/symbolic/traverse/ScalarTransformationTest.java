package org.evolvis.math.symbolic.traverse;

import junit.framework.TestCase;

import org.evolvis.math.symbolic.Scalar;

public class ScalarTransformationTest extends TestCase {

	public void testAll() {
		String rule;
		rule = "x y * --> y x /";
		test(rule, "x y *", "y x /");
		test(rule, "u a b + + p q - *", "p q - u a b + + /");
		
		rule = "x x * --> x";
		test(rule, "x x *", "x");
		test(rule, "y y *", "y");
		test(rule, "z y *", "z y *");
		test(rule, "1 2 + 1 2 + *", "1 2 +");
		
		rule = "2 x * --> 3 x -";
		test(rule, "2 a b + *", "3 a b + -");
		test(rule, "3 a b + *", "3 a b + *");
		
		rule = "n x * --> n 4 +";
		test(rule, "a b *", "a b *");
		test(rule, "1 b *", "1 4 +");
		test(rule, "1", "1");
		
		rule = "x y * --> x x x - +";
		test(rule, "a b *", "a a a - +");
	}
	
	private void test(String rule, String source, String target) {
		ScalarTransformation t = new ScalarTransformation(rule);
		Scalar s = t.apply(Scalar.parse(source));
		assertEquals(Scalar.parse(target), s);
	}
}
