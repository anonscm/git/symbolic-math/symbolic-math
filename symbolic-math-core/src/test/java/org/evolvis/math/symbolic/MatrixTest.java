package org.evolvis.math.symbolic;

import junit.framework.TestCase;

public class MatrixTest extends TestCase {

	public void testGetSubMatrix() throws OperandInitializationException {
		Matrix m = Matrix.create("(1,2,3,4;5,6,7,8;9,10,11,12)");
		assertEquals(m, m.getSubMatrix(0, 0, m.getHeight(), m.getWidth()));
		assertEquals(Matrix.create("(10,11)"), m.getSubMatrix(2, 1, 1, 2));
		assertEquals(Matrix.create("(3;7;11)"), m.getSubMatrix(0, 2, 3, 1));
	}
}
