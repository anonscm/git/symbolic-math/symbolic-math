package org.evolvis.math.symbolic;

import junit.framework.TestCase;

public class ScalarTest extends TestCase {
	
	public void testReplaceInput() {
		Scalar s = (Scalar) Operand.parse("a 7 + c - d e f * / +");
		Scalar r = Number.create("7");
		assertEquals("7", s.replaceNode( 0, r).toString());
		assertEquals("7 d e f * / +", s.replaceNode( 1, r).toString());
		assertEquals("7 c - d e f * / +", s.replaceNode( 2, r).toString());
		assertEquals("7 7 + c - d e f * / +", s.replaceNode( 3, r).toString());
		assertEquals(s, s.replaceNode( 4, r));
		assertEquals("a 7 + 7 - d e f * / +", s.replaceNode( 5, r).toString());
		assertEquals("a 7 + c - 7 +", s.replaceNode( 6, r).toString());
		assertEquals("a 7 + c - 7 e f * / +", s.replaceNode( 7, r).toString());
		assertEquals("a 7 + c - d 7 / +", s.replaceNode( 8, r).toString());
		assertEquals("a 7 + c - d 7 f * / +", s.replaceNode( 9, r).toString());
		assertEquals("a 7 + c - d e 7 * / +", s.replaceNode(10, r).toString());
		assertEquals(s, s.replaceNode(11, r));
		assertEquals(s, s.replaceNode(-1, r));
	}
	

	public void testCompareTo() {
		assertTrue(Number.ZERO.compareTo(Number.ONE) < 0);
		assertTrue(Number.ONE.compareTo(Number.ONE) == 0);
		assertTrue(Number.ONE.compareTo(Number.ZERO) > 0);
		assertTrue(Number.ONE.compareTo(Variable.create("v")) < 0);
		assertTrue(Variable.create("v").compareTo(Variable.create("v")) == 0);
		assertTrue(Variable.create("a").compareTo(Variable.create("b")) < 0);
		assertTrue(Variable.create("b").compareTo(Variable.create("a")) > 0);
		assertTrue(Variable.create("a1").compareTo(Variable.create("a")) > 0);
		assertTrue(Scalar.parse("2 3 +").compareTo(Variable.create("a")) > 0);
		assertTrue(Scalar.parse("2 3 +").compareTo(Scalar.parse("2 3 +")) == 0);
		assertTrue(Scalar.parse("2 3 +").compareTo(Scalar.parse("2 2 +")) > 0);
		assertTrue(Scalar.parse("2 3 +").compareTo(Scalar.parse("2 3 -")) < 0);
		assertTrue(Scalar.parse("2 3 +").compareTo(Scalar.parse("2 3 *")) > 0);
	}
	

	public void testMergeNumbers() {
		Scalar s;
		s = Scalar.parse("x y + 3 2 1 + - *");
		assertEquals(Number.ZERO, s.mergeNumbers());
		s = Scalar.parse("x 2 + z *");
		assertTrue(s == s.mergeNumbers());
		// +
		s = Scalar.parse("0 x y - +");
		assertEquals(Scalar.parse("x y -"), s.mergeNumbers());
		s = Scalar.parse("3 2 +");
		assertEquals(Number.create("5"), s.mergeNumbers());
		// -
		s = Scalar.parse("0 x y + -");
		assertEquals(Scalar.parse("x y + ~"), s.mergeNumbers());
		s = Scalar.parse("x y + 0 -");
		assertEquals(Scalar.parse("x y +"), s.mergeNumbers());
		s = Scalar.parse("3 2 -");
		assertEquals(Number.create("1"), s.mergeNumbers());
		// *
		s = Scalar.parse("1 x y + *");
		assertEquals(Scalar.parse("x y +"), s.mergeNumbers());
		s = Scalar.parse("-1 x y + *");
		assertEquals(Scalar.parse("x y + ~"), s.mergeNumbers());
		s = Scalar.parse("x y + 1 *");
		assertEquals(Scalar.parse("x y +"), s.mergeNumbers());
		s = Scalar.parse("x y + -1 *");
		assertEquals(Scalar.parse("x y + ~"), s.mergeNumbers());
		s = Scalar.parse("0 x y + *");
		assertEquals(Number.ZERO, s.mergeNumbers());
		s = Scalar.parse("x y + 0 *");
		assertEquals(Number.ZERO, s.mergeNumbers());
		// /
		s = Scalar.parse("1 x y + /");
		assertEquals(Scalar.parse("x y + ^-1"), s.mergeNumbers());
		s = Scalar.parse("-1 x y + /");
		assertEquals(Scalar.parse("x y + ^-1 ~"), s.mergeNumbers());
		s = Scalar.parse("x y + 1 /");
		assertEquals(Scalar.parse("x y +"), s.mergeNumbers());
		s = Scalar.parse("x y + -1 /");
		assertEquals(Scalar.parse("x y + ~"), s.mergeNumbers());
		s = Scalar.parse("0 x y + /");
		assertEquals(Number.ZERO, s.mergeNumbers());
		s = Scalar.parse("x y + 0 /");
		assertEquals(Number.NAN, s.mergeNumbers());
	}
}
