package org.evolvis.math.symbolic.function;

import java.util.List;

import junit.framework.TestCase;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Number;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Scalar;
import org.evolvis.math.symbolic.operator.function.Multiplication;

public class MultiplicationTest extends TestCase {

	public void testApplyScalar() {
		assertEquals(Number.create("6"), Multiplication.apply(Number.create("2"), Number.create("3")).mergeNumbers());
	}

	public void testApplyScalarMatrix() {
		Matrix m = Matrix.create("(1,2,3;4,5,6)");
		Matrix mr = Matrix.create("(-1,-2,-3;-4,-5,-6)");
		assertEquals(mr, m.multiply(Number.ONE_NEG).mergeNumbers());
	}

	public void testApplyMatrix() {
		Matrix m1 = Matrix.create("(1,2,3;4,5,6)");
		Matrix m2 = Matrix.create("(6,-1;3,2;0,-3)");
		Matrix mr = Matrix.create("(12,-6;39,-12)");
		
		assertEquals(mr, m1.multiply(m2).mergeNumbers());
	}
	
	public void testSimplification() {
		// 2 3 *  -->  6
		assertEqualsOp("6", "2 3 *");
		// 0 x *  -->  0
		assertEqualsOp("0", "0 x *");
		assertEqualsOp("0", "x 0 *");
		// 1 x *  -->  x
		assertEqualsOp("x", "1 x *");
		assertEqualsOp("x", "x 1 *");
		// -1 x *  -->  x ~
		assertEqualsOp("x ~", "-1 x *");
		assertEqualsOp("x ~", "x -1 *");
	}
	
	private void assertEqualsOp(String str, String strSimp) {
		assertEquals(
				Operand.parse(str),
				Operand.parse(strSimp).mergeNumbers());
	}
/*
	public void testTransformation() throws InterruptedException {
		List<Scalar> sclrs;
		// x y *  -->  y x *
		sclrs = transformTillSize("x y *", 2);
		assertContaines(sclrs, "x y *");
		assertContaines(sclrs, "y x *");
		// x y * z *  -->  z y * x *
		sclrs = transformTillSize("x y * z *", 12);
		assertContaines(sclrs, "x y * z *");
		assertContaines(sclrs, "x y z * *");
		assertContaines(sclrs, "x z * y *");
		assertContaines(sclrs, "x z y * *");
		assertContaines(sclrs, "y x * z *");
		assertContaines(sclrs, "y x z * *");
		assertContaines(sclrs, "y z * x *");
		assertContaines(sclrs, "y z x * *");
		assertContaines(sclrs, "z x * y *");
		assertContaines(sclrs, "z x y * *");
		assertContaines(sclrs, "z y * x *");
		assertContaines(sclrs, "z y x * *");
		// x y + z *  -->  x z * y z * +
		sclrs = transformTillSize("x y + z *", 12);
		assertContaines(sclrs, "x y + z *");
		assertContaines(sclrs, "z x y + *");
		assertContaines(sclrs, "y x + z *");
		assertContaines(sclrs, "z y x + *");
		assertContaines(sclrs, "x z * y z * +");
		assertContaines(sclrs, "x z * z y * +");
		assertContaines(sclrs, "z x * y z * +");
		assertContaines(sclrs, "z x * z y * +");
		assertContaines(sclrs, "y z * x z * +");
		assertContaines(sclrs, "y z * z x * +");
		assertContaines(sclrs, "z y * x z * +");
		assertContaines(sclrs, "z y * z x * +");
		// x y - z *  -->  x z * y z * -
		sclrs = transformTillSize("x y - z *", 6);
		assertContaines(sclrs, "x y - z *");
		assertContaines(sclrs, "z x y - *");
		assertContaines(sclrs, "x z * y z * -");
		assertContaines(sclrs, "x z * z y * -");
		assertContaines(sclrs, "z x * y z * -");
		assertContaines(sclrs, "z x * z y * -");
	}
	*/
	private void assertContaines(List<Scalar> sclrs, String str) {
		assertTrue(sclrs.contains(Operand.parse(str)));
	}
	/*
	private List<Scalar> transformTillSize(String s, int size) {
		ScalarCostCalculator scc = new SizeScalarCostCalculator();
		ScalarTransformer tf = new ScalarTransformer();
		tf.start((Scalar) Operand.parse(s), scc, false);
		while (tf.isRunning())
			if (tf.getSize() >= size)
				tf.stop();
		assertEquals(size, tf.getSize());
		List<Scalar> list = new LinkedList<Scalar>();
		for (int i=0; i<tf.getSize(); i++)
			list.add(tf.get(i));
		return list;
	}*/
}
