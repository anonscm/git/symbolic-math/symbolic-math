package org.evolvis.math.symbolic;

import junit.framework.TestCase;

public class IncrementalMatrixInitializerTest extends TestCase {
	
	public void testRight() throws OperandInitializationException {
		IncrementalMatrixInitializer m = new IncrementalMatrixInitializer();
		m.addElement(Number.create("1"));
		m.addElement(Number.create("2"));
		m.endRow();
		m.addElement(Number.create("3"));
		m.addElement(Number.create("4"));
		m.endRow();
		m.addElement(Number.create("5"));
		m.addElement(Number.create("6"));
		// m.endRow(); // should make no difference
		Matrix mtrx = m.endMatrix();
		
		assertTrue(mtrx.isImmutable());
		assertEquals(2, mtrx.getWidth());
		assertEquals(3, mtrx.getHeight());
		assertEquals(Matrix.create("(1,2;3,4;5,6)"), mtrx);
	}
	
	public void testTooLongRow() throws OperandInitializationException {
		IncrementalMatrixInitializer m = new IncrementalMatrixInitializer();
		m.addElement(Number.create("1"));
		m.addElement(Number.create("2"));
		m.endRow();
		m.addElement(Number.create("3"));
		m.addElement(Number.create("4"));
		try {
			m.addElement(Number.create("5"));
			fail();
		} catch (RuntimeException e) {
			//ok
		}
	}
	
	public void testTooShortRow() throws OperandInitializationException {
		IncrementalMatrixInitializer m = new IncrementalMatrixInitializer();
		m.addElement(Number.create("1"));
		m.addElement(Number.create("2"));
		m.endRow();
		m.addElement(Number.create("3"));
		try {
			m.endRow();
			fail();
		} catch (RuntimeException e) {
			//ok
		}
		
		m = new IncrementalMatrixInitializer();
		try {
			m.endMatrix();
			fail();
		} catch (RuntimeException e) {
			//ok
		}
	}
}
