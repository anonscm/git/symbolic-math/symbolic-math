package org.evolvis.math.symbolic.traverse;

import junit.framework.TestCase;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.MatrixOperations;
import org.evolvis.math.symbolic.operator.Ones;

public class MatrixTransformerTest extends TestCase {

	
	public void _testK() {
		//TODO to slow!
		testOnesMatrix1(Matrix.create("(c, c s *, x; 0, 1 m + c *, y; 0, 0, 1)"));
	}
	
	public void _test2() {
		//TODO does not work right now => more rules?
		testOnesMatrix1(Matrix.create("(a,b;c,d)"));
	}
	
	public void _test3() {
		//TODO does not work right now => more rules?
		testOnesMatrix1(Matrix.create("(a,0,b;0,c,0;0,d,e)"));
	}
	
	public void _testOnesMatrix1() {
		testOnesMatrix1(Matrix.create("(a, 0, 0; 0, 0, 1; a, 2, b)"));
	}
	
	public void test1dMatrix() {
		testOnesMatrix1(Matrix.create("(a)"));
	}
	
	public void _testInvertSpd() {
		Matrix A = Matrix.create("(a, 1; 0, c)");
		Matrix SPD = A.transpose().multiply(A);
		Matrix SPDi = MatrixOperations.invertSPD(SPD);

		testOnesMatrix2(SPD.multiply(SPDi));
	}
	
	private void testOnesMatrix1(Matrix m) {
		Matrix mi = m.invert();
		Matrix I = mi.multiply(m);
		testOnesMatrix2(I);
		I = m.multiply(mi);
		testOnesMatrix2(I);
	}
	
	private void testOnesMatrix2(Matrix m) {
		System.out.println(m);
		MatrixTransformer mtr = new MatrixTransformer();
		mtr.addUpdateListener(new TransformerUpdateListener<Matrix>() {
			long time = System.currentTimeMillis();
			public void newTransformation(Matrix transformation, double cost) {
				if (System.currentTimeMillis() - time > 20000 || cost < 50) {
					System.out.println(transformation);
					System.out.println("found new formula with cost: "+cost);
					time = System.currentTimeMillis();
				} else
					System.out.println("found new formula with cost: "+cost);
			}
		});
		mtr.start(m, true);
		for (int i=0; mtr.isRunning(); i++)
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		Matrix T = mtr.getBest();
		System.out.println(T);
		assertEquals(Ones.apply(m.getHeight()), T);
	}
}