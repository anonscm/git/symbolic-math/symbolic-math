package org.evolvis.math.symbolic;

import java.util.Collection;
import java.util.Iterator;

import junit.framework.TestCase;

public class FunctionTypeTest extends TestCase {

	public void testGetInputCount() {
		Collection<FunctionType> types = FunctionType.getFunctionTypes();
		Iterator<FunctionType> it = types.iterator();
		while (it.hasNext()) {
			FunctionType type = it.next();
			assertTrue(type.getInputCount() >= 1);
		}
	}
	
	public void testEqualsAndHashCode() {
		Collection<FunctionType> types = FunctionType.getFunctionTypes();
		assertTrue("need at least two types to compare", types.size() >= 2);
		Iterator<FunctionType> it = types.iterator();
		// get first type
		FunctionType type0 = it.next();
		// does the equals method work correctly?
		assertTrue(type0.equals(type0));
		// does the hashCode method work correctly?
		assertEquals(type0.hashCode(), type0.hashCode());
		// compare all other types with first type
		while (it.hasNext()) {
			FunctionType type = it.next();
			// does the equals method work correctly?
			assertTrue(type.equals(type));
			assertFalse(type.equals(type0));
			assertFalse(type0.equals(type));
			// does the hashCode method work correctly?
			assertEquals(type.hashCode(), type.hashCode());
		}
	}
}
