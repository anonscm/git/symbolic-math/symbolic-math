package org.evolvis.math.symbolic;


import junit.framework.TestCase;

import org.evolvis.math.symbolic.operator.Ones;

public class MatrixOperationsTest extends TestCase {

	public void testGaussJordan() {
		Matrix m = Matrix.create("(1,2,3, 1,0,0; 4,5,6, 0,1,0; 7,8,-9, 0,0,1)");
		Matrix m2 = MatrixOperations.gaussJordan(m);
		Matrix A = m.getSubMatrix(0,0,3,3);
		Matrix Ainv = m2.getSubMatrix(0, 3, 3, 3);
		Matrix I = A.multiply(Ainv).mergeNumbers();
		assertEquals(Ones.apply(3), I);
	}
	
	public void testInvert() {
		Matrix A = Matrix.create("(0,2,3; 2,0,3; 1,1,1)");
		Matrix Ainv = A.invert();
		Matrix I = A.multiply(Ainv).mergeNumbers();
		assertEquals(Ones.apply(3), I);
	}
	
	public void testSolveSPD() {
		// random Matrix
		Matrix A = Matrix.create("(0,2,3; 2,1,3; 1,1,1)");
		Matrix M = A.transpose().multiply(A);
		// random matrix with the same height as matrix M
		Matrix Y = Matrix.create("(1, -5, 6, 0; 7, 9, 9, 1; -33, 1.2, 4, 66)");
		Matrix MinvY1 = MatrixOperations.solveSPD(M, Y);
		Matrix MinvY2 = M.invert().multiply(Y);
		assertEquals(MinvY2.mergeNumbers(), MinvY1.mergeNumbers());
	}
	
	public void testForwardSubstitution() {
		// U: any square matrix with ones on and zeros below the diagonal
		Matrix U = Matrix.create("(1, -2, 3, -4; 0, 1, -7, 9; 0, 0, 1, 12; 0, 0, 0, 1)");
		// Y: any matrix which must have the same height as U
		Matrix Y = Matrix.create("(4, 3, 2; 5, -7, 1; 3, 9, 0; 8, -12, 5)");
		// calculate X = U ^-T * Y
		Matrix X = U.transpose().invert().multiply(Y);
		Matrix X2 = MatrixOperations.forwardSubstitution(U, Y);
		assertEquals(X.mergeNumbers(), X2.mergeNumbers());
	}

	public void testBackwardSubstitution() {
		// U: any square matrix with ones on and zeros below the diagonal
		Matrix U = Matrix.create("(1, -2, 3, -4; 0, 1, -7, 9; 0, 0, 1, 12; 0, 0, 0, 1)");
		// Y: any matrix which must have the same height as U
		Matrix Y = Matrix.create("(4, 3, 2; 5, -7, 1; 3, 9, 0; 8, -12, 5)");
		// calculate X = U ^-1 * Y
		Matrix X = U.invert().multiply(Y);
		Matrix X2 = MatrixOperations.backwardSubstitution(U, Y);
		assertEquals(X.mergeNumbers(), X2.mergeNumbers());
	}


	public void testSolveDiagonal() {
		// U: any square diagonal matrix with zeros below the diagonal
		Matrix D = Matrix.create("(-7, 0, 0, 0, 0; 0, 2, 0, 0, 0; 0, 0, 5, 0, 0; 0, 0, 0, 56, 0; 0, 0, 0, 0, -2)");
		// Y: any matrix which must have the same height as U
		Matrix Y = Matrix.create("(4, 3, 2; 5, -7, 1; 3, 9, 0; 8, -12, 5; 44, -21, 99)");
		// calculate X = D ^-1 * Y
		Matrix X = D.invert().multiply(Y);
		Matrix X2 = MatrixOperations.solveDiagonal(D, Y);
		assertEquals(X.mergeNumbers(), X2.mergeNumbers());
	}
}
