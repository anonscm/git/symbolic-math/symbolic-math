package org.evolvis.util;

import java.util.LinkedList;
import java.util.List;

public class Stack<T> {

	private List<T> stack;
	
	public void push(T element) {
		if (stack == null)
			stack = new LinkedList<T>();
		stack.add(element);
	}
	
	public T pop() {
		if (stack == null || stack.isEmpty())
			return null;
		return stack.remove(stack.size()-1);
	}
	
	public boolean isEmpty() {
		return stack == null || stack.size() == 0;
	}
}
