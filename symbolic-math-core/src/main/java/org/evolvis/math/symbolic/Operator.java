package org.evolvis.math.symbolic;

import java.util.LinkedList;
import java.util.List;

import org.evolvis.math.symbolic.operator.Concat;
import org.evolvis.math.symbolic.operator.Differentiation;
import org.evolvis.math.symbolic.operator.Dimension;
import org.evolvis.math.symbolic.operator.InvertSpd;
import org.evolvis.math.symbolic.operator.Ones;
import org.evolvis.math.symbolic.operator.Optimize;
import org.evolvis.math.symbolic.operator.QuaternionConjugation;
import org.evolvis.math.symbolic.operator.QuaternionMultiplication;
import org.evolvis.math.symbolic.operator.Replace;
import org.evolvis.math.symbolic.operator.SolveSpd;
import org.evolvis.math.symbolic.operator.SubMatrix;
import org.evolvis.math.symbolic.operator.Transpose;
import org.evolvis.math.symbolic.operator.function.Addition;
import org.evolvis.math.symbolic.operator.function.Cosinus;
import org.evolvis.math.symbolic.operator.function.Division;
import org.evolvis.math.symbolic.operator.function.Inversion;
import org.evolvis.math.symbolic.operator.function.Multiplication;
import org.evolvis.math.symbolic.operator.function.Negation;
import org.evolvis.math.symbolic.operator.function.Sinus;
import org.evolvis.math.symbolic.operator.function.Subtraction;
import org.evolvis.math.symbolic.operator.function.condition.EqualTo;
import org.evolvis.math.symbolic.operator.function.condition.GreaterTo;
import org.evolvis.math.symbolic.operator.function.condition.IfElse;
import org.evolvis.math.symbolic.operator.function.condition.LowerTo;
import org.evolvis.util.Stack;

public abstract class Operator {

	protected static final RuntimeException EXCEPTION_WRONG_NUMBER_OF_INPUT_PARAMATERS = new RuntimeException("wrong number of input parameters");
	protected static final RuntimeException EXCEPTION_UNKNOWN_OPERATOR_NAME = new RuntimeException("unknown operator");
	public static final RuntimeException EXCEPTION_MATRIX_DIMENSION = new RuntimeException("matrix size does not fit");
	protected static final RuntimeException EXCEPTION_WRONG_INPUT_TYPE = new RuntimeException("wrong input parameter type");
	protected static final RuntimeException EXCEPTION_INVALID_OPERATOR = new RuntimeException("invalid operator");
	
	private static List<Operator> operators = new LinkedList<Operator>();
	
	static {
		loadOperators();
	}
	
	protected Operator() {
		validate();
	}

	private void validate() {
		if (getInputCount() < 1)
			throw EXCEPTION_INVALID_OPERATOR;
		operators.add(this);
	}
	
    private static void loadOperators() {
    	new Addition();
    	new Cosinus();
    	new Division();
    	new Inversion();
    	new Multiplication();
    	new Negation();
    	new Sinus();
    	new Subtraction();
    	
    	// conditional functions
    	new EqualTo();
    	new GreaterTo();
    	new LowerTo();
    	new IfElse();
    	
    	// by Operators which are not FunctionTypes the order can be relevant
    	// (if some accept the same token by #matchToken(String), the first in
    	// the list will be used)
    	new Concat();
    	new Differentiation();
    	new Dimension();
    	new Ones();
    	new Optimize();
    	new QuaternionConjugation();
    	new QuaternionMultiplication();
    	new Replace();
    	new SolveSpd();
    	new InvertSpd();
    	new SubMatrix();
    	new Transpose();
    }
    
	/**
	 * >= 1
	 * 
	 * @return
	 */
	public abstract int getInputCount();
	
	private static final Operator getOperator(String token) {
		for (Operator op : operators)
			if (op.matchToken(token))
				return op;
		return null;
	}
	
	protected abstract boolean matchToken(String token);
	
	/**
	 * 
	 * 
	 * @param  token
	 *         For interpretation if the operator for example contains
	 *         parameters
	 * @param  input
	 *         It will be assured, that the list does have the size {@link #getInputCount()}.
	 *         The list does not contain {@link Matrix} instances of the dimension 1x1.
	 * @return
	 */
	protected abstract Operand apply(String token, Operand... input);
	
	public static void applyOperator(String token, Stack<Operand> stack) {
		Operator op = getOperator(token);
		if (op == null)
			throw EXCEPTION_UNKNOWN_OPERATOR_NAME;
		Operand[] input = new Operand[op.getInputCount()];
		for (int i=op.getInputCount()-1; i>=0; i--)
			if (stack.isEmpty())
				throw EXCEPTION_WRONG_NUMBER_OF_INPUT_PARAMATERS;
			else
				input[i] = stack.pop().tryScalar();
		stack.push(op.apply(token, input));
	}

	public static boolean isOperator(String token) {
		for (Operator op : operators)
			if (op.matchToken(token))
				return true;
		return false;
	}
}
