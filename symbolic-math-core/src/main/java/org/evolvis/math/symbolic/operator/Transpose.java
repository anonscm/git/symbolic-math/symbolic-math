package org.evolvis.math.symbolic.operator;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Operator;
import org.evolvis.math.symbolic.Scalar;

public class Transpose extends Operator {

	private static final String ID = "^t";

	@Override
	protected boolean matchToken(String token) {
		return ID.equals(token);
	}

	@Override
	public int getInputCount() {
		return 1;
	}

	@Override
	protected Operand apply(String token, Operand... input) {
		return apply(input[0]);
	}

	public static final Operand apply(Operand op) {
		if (op instanceof Scalar)
			return op;
		return apply((Matrix) op);
	}

	public static final Matrix apply(Matrix A) {
		if (A.isScalar())
			return A;
		int width  = A.getWidth();
		int height = A.getHeight();
		Matrix matrixT = new Matrix(width, height);
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				matrixT.setElement(j, i, A.getElement(i, j));
		matrixT.setImmutable();
		return matrixT;
	}
}