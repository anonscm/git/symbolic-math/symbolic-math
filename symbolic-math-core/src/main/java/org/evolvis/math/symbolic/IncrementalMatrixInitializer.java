package org.evolvis.math.symbolic;

import java.util.LinkedList;
import java.util.List;

/**
 * Useful for deserialization.
 * 
 * @author Hendrik Helwich
 */
public class IncrementalMatrixInitializer {
	
	/** The matrix which will be used in the end to store the data. */
	private Matrix matrix;

	/** This will be set when {@link #endMatrix()} is called. */
	private int height;

	/** This will be set when {@link #endRow()} is called. */
	private int width;
	
	/** Will be used for the first row because the matrix width is unknown. */
	private List<Scalar> firstRowList;
	
	/** Will be used for all rows execept the first. The matrix width is known.
	 */
	private Scalar[] currentRow;
	
	/** Will be used to store all matrix rows. It is a {@link List} because the
	 *  matrix height is unknown. */
	private List<Scalar[]> rowList;
	
	/** <code>true</code> if the current row is the first row of the matrix. */
	private boolean firstRow = true;
	
	/** The current column index for all rows execpt the first one. */
	private int j = 0;
	
	/**
	 * Add a {@link Scalar} to the current row of the matrix.
	 * 
	 * @param  s
	 */
	public void addElement(Scalar s) {
		if (matrix != null)
			throw new RuntimeException("matrix is already created");
		if (s == null)
			throw new NullPointerException();
		if (firstRow) {
			if (firstRowList == null)
				firstRowList = new LinkedList<Scalar>();
			firstRowList.add(s);
		} else {
			if (j < width) {
				if (currentRow == null)
					currentRow = new Scalar[width];
				currentRow[j++] = s;
			} else
				throw new RuntimeException("matrix column out of bounce");
		}
	}
	
	/**
	 * Start a new matrix row.
	 */
	public void endRow() {
		if (matrix != null)
			throw new RuntimeException("matrix is already created");
		if (firstRow) {
			if (firstRowList == null)
				throw new RuntimeException("matrix row is empty");
			width = firstRowList.size();
			assert(width > 0); // because firstRowList will be initialized and
			                   // a first element will be added in one step
			Scalar[] row = (Scalar[]) firstRowList.toArray(new Scalar[0]);
			rowList = new LinkedList<Scalar[]>();
			rowList.add(row);
			firstRowList = null; // free for garbage collection
			firstRow = false;
		} else {
			if (currentRow == null)
				throw new RuntimeException("previous matrix row is empty");
			if (j != width)
				throw new RuntimeException("matrix row is to short");
			rowList.add(currentRow);
			currentRow = null;
			j = 0;
		}
	}

	public Matrix endMatrix() {
		if (matrix != null)
			throw new RuntimeException("matrix is already created");
		if (firstRowList != null || currentRow != null)
			endRow();
		if (rowList == null)
			throw new RuntimeException("matrix is empty");
		height = rowList.size();
		assert(height > 0); // because rowList will be initialized and
                            // a first element will be added in one step
		Scalar[][] data = (Scalar[][]) rowList.toArray(new Scalar[0][0]);
		rowList = null; // free for garbage collection
		matrix = new Matrix(data);
		return matrix;
	}
}
