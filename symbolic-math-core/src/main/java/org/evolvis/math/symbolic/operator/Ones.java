package org.evolvis.math.symbolic.operator;

import java.math.BigInteger;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Number;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Operator;

public class Ones extends Operator {

	private static final String ID = "ones";

	@Override
	protected boolean matchToken(String token) {
		return ID.equals(token);
	}

	@Override
	public int getInputCount() {
		return 1;
	}
	
	/**
	 * The input parameter needs to be an instance of {@link Number} which must
	 * be an integer which is greater than zero.
	 * 
	 * @see org.evolvis.math.symbolic.Operator#applyOperator(String, org.evolvis.util.Stack)
	 */
	@Override
	protected Operand apply(String token, Operand... input) {
		if (! (input[0] instanceof Number))
			throw EXCEPTION_WRONG_INPUT_TYPE;
		Number size = (Number) input[0];
		if (! size.isIntegral() || size.getNominator().compareTo(BigInteger.ONE) < 0)
			throw EXCEPTION_WRONG_INPUT_TYPE;
		return apply(size.getNominator().intValue());
	}

	public static final Matrix apply(int size) {
		Matrix ones = new Matrix(size, size);
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				ones.setElement(i, j, i == j ? Number.ONE : Number.ZERO);
		return ones.setImmutable();
	}
}