package org.evolvis.math.symbolic.environment;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Variable;

/**
 * An immutable data structure which stores the index of a {@link Matrix}
 * element.
 * It implements {@link Comparable}, so it is possible to sort a list of
 * {@link MatrixIndex} objects. This class will be used by the class
 * {@link VariableMatrixReference} to store all positions of a {@link Variable}
 * in a {@link Matrix}.
 * 
 * @author Hendrik Helwich
 */
class MatrixIndex implements Comparable<MatrixIndex> {
	
	private final int row, column;
	
	public MatrixIndex(int row, int column) {
		if (row < 0 || column < 0)
			throw new IllegalArgumentException();
		this.row = row;
		this.column = column;
	}
	
	public int getColumn() {
		return column;
	}
	
	public int getRow() {
		return row;
	}
	
	public int compareTo(MatrixIndex mp) {
		if (row == mp.row)
			return column - mp.column;
		return row - mp.row;
	}

	@Override
	public String toString() {
		return "("+row+","+column+")";
	}
}
