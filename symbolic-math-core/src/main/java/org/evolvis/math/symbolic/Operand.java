package org.evolvis.math.symbolic;

import java.util.Collection;
import java.util.HashSet;

import org.evolvis.math.symbolic.environment.OperandEnvironment;


/**
 * An immutable object which can be either a {@link Scalar} or a
 * {@link Matrix} object.
 * 
 * @author Hendrik Helwich
 *
 */
public abstract class Operand {

	protected static final RuntimeException IMMUTABLE_IS_READONLY =
		new RuntimeException("it is not allowed to write to an immutable operand");
	
	protected static final RuntimeException IMMUTABLE_EXPECTED =
		new RuntimeException("an immutable operand is expected");

	protected static final RuntimeException SERIALIZATION_WRONG_TYPE =
		new RuntimeException("serialization does result in a different type");
	
	protected boolean isImmutable = false;
	
	public boolean isImmutable() {
		return isImmutable;
	}

	public Operand setImmutable() {
		if (isImmutable) //TODO mostly for debugging
			throw new RuntimeException("operand is immutable already");
		isImmutable = true;
		return this;
	}
	
	public static Operand create(String str) {
		return OperandSerializer.parseOperand(str);
	}

	@Override
	public abstract boolean equals(Object obj);

	@Override
	public abstract int hashCode();
	
	@Override
	public String toString() {
		return OperandSerializer.toString(this);
	}
	
	public static Operand parse(String str) {
		return OperandSerializer.parseOperand(str);
	}
	
	public static Operand parse(OperandEnvironment env, String str) {
		return OperandSerializer.parseOperand(env, str);
	}
	
	public Operand tryScalar() {
		if (this instanceof Matrix) {
			Matrix m = (Matrix) this;
			if (m.isScalar())
				return m.getElement(0, 0);
		}
		return this;
	}
	
	public Matrix asMatrix() {
		if (this instanceof Scalar)
			return new Matrix((Scalar) this);
		return (Matrix) this;
	}

	public abstract Operand mergeNumbers();
	
	/**
	 * Returns the number of nodes in the tree which are an instance of
	 * {@link Scalar}.
	 * The returned value must be >= 1.
	 * 
	 * @return
	 */
	public abstract int getSize();

	protected abstract void getVariables(Collection<Variable> variables);
	
	public Collection<Variable> getVariables() {
		Collection<Variable> vars = new HashSet<Variable>(); // HahSet eliminates duplicates
		getVariables(vars);
		return vars;
	}
}
