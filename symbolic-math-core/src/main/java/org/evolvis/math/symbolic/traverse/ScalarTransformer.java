package org.evolvis.math.symbolic.traverse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.evolvis.math.symbolic.Function;
import org.evolvis.math.symbolic.Number;
import org.evolvis.math.symbolic.OperandInitializationException;
import org.evolvis.math.symbolic.Scalar;
import org.evolvis.math.symbolic.Variable;

public class ScalarTransformer implements Runnable {

    private static final int EXPANDING_STEPS_COUNT = 1;

	/**
     * A logger.
     */
    private static final transient Logger logger = Logger.getLogger(ScalarTransformer.class.getName());
    
    /**
     * The name of the file which specfies the constants. If the filename is
     * relative it will be loaded in relation to the folder of this class.
     */
    private static final String FILENAME = "transformations.txt";
    
    private static int firstErrorLine = -1;

    private static ScalarTransformation[] transformations = loadTransformations();
    

	private int MAX_BEST_SCALARS_COUNT = 800;
	private int MAX_RANDOM_SCALARS_COUNT = 200;

    
	private Thread thread;
	private boolean stopped;
	private SortedScalarList bestScalars;
	private BoundedScalarArray randomScalars;
	private boolean checkMinimal;
	private OperandCostCalculator scc;
	private double bestScalarCost;
	private Scalar bestScalar;

	private Scalar node;

	public ScalarTransformer(Scalar node, boolean checkMinimal) {
		this(node, checkMinimal, new CalculationErrorCostCalculator());
	}

	public ScalarTransformer(Scalar node, boolean checkMinimal, OperandCostCalculator scc) {
		if (node == null)
			throw new NullPointerException();
		this.node = node.mergeNumbers();
		this.checkMinimal = checkMinimal;
		this.scc = scc;
	}
	
	private List<TransformerUpdateListener<Scalar>> updateListener;

	public void addUpdateListener(TransformerUpdateListener<Scalar> listener) {
		if (listener != null) {
			if (updateListener == null)
				updateListener = new LinkedList<TransformerUpdateListener<Scalar>>();
			updateListener.add(listener);
		}
	}
	
	private void notifyUpdateListener() {
		if (updateListener != null)
			for (TransformerUpdateListener<Scalar> listener : updateListener)
				listener.newTransformation(bestScalar, bestScalarCost);
	}
	
	private static ScalarTransformation[] loadTransformations() {
        List<ScalarTransformation> transfList = new LinkedList<ScalarTransformation>();
        final InputStream inStream = ScalarTransformer.class.getResourceAsStream(FILENAME);
        final Reader reader = new InputStreamReader(inStream);
        final BufferedReader br = new BufferedReader(reader);
        if (inStream == null) {
            logger.severe("cannot find transformation file: " + FILENAME);
        } else {
            try {
            	String tstr;
        		Pattern PATTERN_EMPTY_LINE = Pattern.compile("\\s*");
            	for (int line = 1; (tstr = br.readLine()) != null; line ++) {
            		// cut out comments
            		int cidx = tstr.indexOf('#');
            		if (cidx != -1)
            			tstr = tstr.substring(0, cidx);
            		if (! PATTERN_EMPTY_LINE.matcher(tstr).matches()) {// ignore empty lines
						try {
							ScalarTransformation st = new ScalarTransformation(tstr);
	            			if (! transfList.contains(st))
	            				transfList.add(st);
						} catch (Exception e) {
							logger.log(Level.SEVERE, "Error in transformation file in line "+line, e);
							if (firstErrorLine == -1)
								firstErrorLine = line;
						}						
            		}
            	}
                return transfList.toArray(new ScalarTransformation[] {});
            } catch (IOException e) {
                logger.log(Level.SEVERE,
                        "error reading transformation file: " + FILENAME, e);
            } catch (OperandInitializationException e) {
                logger.log(Level.SEVERE,
                        "error parsing file: " + FILENAME, e);
			}
        }
		return null;
	}
	
	public static int getFirstErrorLine() {
        return firstErrorLine;
	}

	public synchronized void start() {
		if (thread != null)
			throw new RuntimeException("thread is already running");
		bestScalars = new SortedScalarList(MAX_BEST_SCALARS_COUNT);
		bestScalars.add(node);
		randomScalars = new BoundedScalarArray(MAX_RANDOM_SCALARS_COUNT);
		bestScalarCost = scc.getCost(node);
		if (! VALIDATE_TRANSFORMATION)
			calculateValidataionReference(node);
		if (!checkMinimal || !isMinimal(node)) {
			stopped = false;
			thread = new Thread(this);
			thread.start();
		}
	}
	
	public boolean isRunning() {
		return thread != null;
	}
	
	public void stop() {
		stopped = true;
	}

	public void run() {
		try {
			Random random = new Random();
			int MAX_NODE_TRANSFORM_COUNT = 10;
			boolean takeBest = false;
			if (! node.isAtomic())
				while (! stopped) {
					// get random formula
					Scalar formula;
					if (takeBest || randomScalars.getSize() == 0) {
						formula = bestScalars.getRandom();
						takeBest = false;
					} else {
						formula = randomScalars.getRandom();
						takeBest = true;
					}
					// take some random nodes and expand them
					int size = formula.getSize();
					int count = size > 1 ? size >> 1 : 1;
					if (count > MAX_NODE_TRANSFORM_COUNT)
						count = MAX_NODE_TRANSFORM_COUNT;
					
					for (; count >= 0; count--) {
						// expand random node
						int j = random.nextInt(size);
						Scalar s = formula.selectNode(j);
						if (s instanceof Function) {
							Function func = (Function) s;
							Set<Scalar> tfs = transform(func);
							// iterate over new formulas
							for (Scalar formula2 : tfs) {
								formula2 = formula.replaceNode(j, formula2);
								try {
									formula2 = formula2.mergeNumbers();
								} catch (Exception e) {
									// a rule produced an illegal formula!
									logger.info("skip illegal formula "+formula2);
									continue;
								}							
								if (!bestScalars.add(formula2))
									randomScalars.add(formula2);
								double cost = scc.getCost(formula2);
								boolean notify = false;
								if (cost < bestScalarCost) {
									bestScalarCost = cost;
									bestScalar = formula2;
									notify = true;
								}
								if (formula2.isAtomic() || (checkMinimal && isMinimal(formula2))) {
									stopped = true;
									notify = true;
								}
								if (notify)
									notifyUpdateListener();
							}
						}
					}
				}
			if (! VALIDATE_TRANSFORMATION)
				validateScalar(bestScalar, null, null);
		} finally {
			thread = null;
		}
	}
	
	/**
	 * true:  validate every derived formula      (n times)
	 * false: validate only the resulting formula (1 time) 
	 */
	private static final boolean VALIDATE_TRANSFORMATION = false;
	
	private static Random random = new Random();
	
	private static Number getRandomTestNumber() {
		Number n;
		do 
			n = Number.create(
				BigInteger.valueOf(random.nextInt(3)-1), 
				BigInteger.valueOf(random.nextInt(3)-1));
		while (Number.NAN.equals(n) || Number.ZERO.equals(n));
		return n;
	}
	

	private Map<Variable, Number> referenceParams = null;
	private Scalar referenceValue = null;
	
	private void calculateValidataionReference(Scalar scalar) {
		if (VALIDATE_TRANSFORMATION) {
			// get all used variables
			Collection<Variable> vars = scalar.getVariables();
			for (int i = 0; i == 0 || Number.NAN.equals(referenceValue); i ++) {
				if (i == 20) // maybe function result is undefined
					throw new RuntimeException("cannot create valid test parameters");
				// get random parameters 
				referenceParams = new HashMap<Variable, Number>();	
				for (Variable v : vars)
					referenceParams.put(v, getRandomTestNumber());
				// calculate value
				referenceValue = scalar;
				for (Variable v : referenceParams.keySet())
					referenceValue = referenceValue.replaceScalar(v, referenceParams.get(v));
				referenceValue = referenceValue.mergeNumbers();
			}
		}
	}
	
	private void validateScalar(Scalar s, Scalar scalar, ScalarTransformation t) {
		if (referenceParams != null) { // replace all variables with numbers and calculate scalar 
			Scalar test = s;
			for (Variable v : referenceParams.keySet())
				test = test.replaceScalar(v, referenceParams.get(v));
			Scalar n = test.mergeNumbers();
			if (! referenceValue.equals(n) && ! Number.NAN.equals(n)) {
				if (t != null)
					System.out.println("transformation "+t+" maybe is invalid");
				if (scalar != null)
					System.out.println("initial formula: "+scalar);
				System.out.println("formula: "+s);
				System.out.println("value: "+referenceValue+ " != "+n +" = "+test);
			}
		}
	}
	
	private Set<Scalar> transform(Scalar scalar) {
		// create optional variables for validation
		calculateValidataionReference(scalar);
		
		Set<Scalar> trfList = new HashSet<Scalar>(); // or TreeSet?
		trfList.add(scalar);
		for (int size = -1, i = 1; trfList.size() != size; i++) {
			size = trfList.size();
			transform(trfList, i <= EXPANDING_STEPS_COUNT); // use expanding rules only a constant
			                            // number of steps (here: only first) 
		}
		trfList.remove(scalar);
		return trfList;
	}
	
	private void transform(Set<Scalar> trfList, boolean expanding) {
		Iterator<Scalar> it = trfList.iterator();
		Set<Scalar> trfList2 = new HashSet<Scalar>(); // or TreeSet?
		while (it.hasNext()) {
			Scalar scalar = it.next();
			for (ScalarTransformation t : transformations) {
				if (expanding || !t.isExpanding()) {
					Scalar s = t.apply(scalar);
					// optional validation of the transformed scalar
					validateScalar(s, scalar, t);
					
					trfList2.add(s);
				}
			}
		}
		trfList.addAll(trfList2);
	}

	private boolean isMinimal(Scalar scalar) { //TODO more minimal cases
		// check if no duplicate variables and max no number
		List<String> vars = new LinkedList<String>();
		if (checkVariables(vars, scalar))
			return true;
		return false;
	}
	
	private boolean checkVariables(List<String> vars, Scalar scalar) {
		if (scalar instanceof Variable) {
			String name = ((Variable) scalar).getName();
			if (vars.contains(name))
				return false;
			vars.add(name);
			return true;
		} else if (scalar instanceof Number) {
			return false; //TODO make differences (one number should be allowed)
		} else {
			Function func = (Function) scalar;
			for (int i=0; i<func.getInputCount(); i++)
				if (!checkVariables(vars, func.getInput(i)))
					return false;
			return true;
		}
	}

	public Scalar getBest() {
		return bestScalar;
	}
}
