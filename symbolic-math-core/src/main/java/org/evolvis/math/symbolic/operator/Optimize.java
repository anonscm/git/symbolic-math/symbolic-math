package org.evolvis.math.symbolic.operator;

import java.util.Scanner;

import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Operator;
import org.evolvis.math.symbolic.traverse.OperandTransformer;
import org.evolvis.math.symbolic.traverse.TransformerUpdateListener;

/**
 * singleton
 * 
 * @author Hendrik Helwich
 *
 */
public class Optimize extends Operator {
	
	private static final String ID = "opt";

	@Override
	protected boolean matchToken(String token) {
		return ID.equals(token);
	}

	@Override
	public int getInputCount() {
		return 1;
	}

	@Override
	protected Operand apply(String token, Operand... input) {
		return apply(input[0]);
	}
	
	public static final Operand apply(Operand op) {
		//System.out.println(op);
		OperandTransformer tr = new OperandTransformer();
		tr.addUpdateListener(new TransformerUpdateListener<Operand>() {
			public void newTransformation(Operand transformation, double cost) {
				//if (cost < 50)
				//	System.out.println(transformation);
				System.out.println("found new node with cost: "+cost);
			}
		});
		tr.start(op, false);
		for (int i=0; tr.isRunning(); i++)
			try {
				if (i == 100) {
					Scanner in = new Scanner(System.in);
					if (in.hasNext()) {
						tr.stop();
						in.next();
					}
					i = 0;
				}
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		op = tr.getBest();
		//System.out.println(op);
		return op;
	}
}
