package org.evolvis.math.symbolic.operator;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Operator;

public class QuaternionConjugation extends Operator {

	public static final String ID = "q^-1";

	@Override
	protected boolean matchToken(String token) {
		return ID.equals(token);
	}

	@Override
	public int getInputCount() {
		return 1;
	}

	@Override
	protected Operand apply(String token, Operand... input) {
		return apply(input[0].asMatrix());
	}

	public static final Matrix apply(Matrix q1) {
		if (! QuaternionMultiplication.isQuaternion(q1))
			throw EXCEPTION_MATRIX_DIMENSION;
		Matrix q = new Matrix(4, 1);
		q.setElement(0, 0, q1.getElement(0, 0));
		q.setElement(1, 0, q1.getElement(1, 0).negate());
		q.setElement(2, 0, q1.getElement(2, 0).negate());
		q.setElement(3, 0, q1.getElement(3, 0).negate());
		return q.setImmutable();
	}
}
