package org.evolvis.math.symbolic.operator;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Operator;

public class Concat extends Operator {

	private static final String ID_VERTICAL = "\\";
	private static final String ID_HORIZONTAL = "|";

	@Override
	protected boolean matchToken(String token) {
		return ID_VERTICAL.equals(token) || ID_HORIZONTAL.equals(token);
	}

	@Override
	public int getInputCount() {
		return 2;
	}

	@Override
	protected Operand apply(String token, Operand... input) {
		if (ID_VERTICAL.equals(token))
			return applyVertical(input[0].asMatrix(), input[1].asMatrix());
		return applyHorizontal(input[0].asMatrix(), input[1].asMatrix());
	}

	public static final Matrix applyHorizontal(Matrix A, Matrix B) {
		int height = A.getHeight();
		if (height != B.getHeight())
			throw EXCEPTION_MATRIX_DIMENSION;
		int widthA = A.getWidth();
		int widthB = B.getWidth();
		Matrix C = new Matrix(height, widthA + widthB);
		for (int i = 0; i < height; i ++) {
			for (int j = 0; j < widthA; j ++)
				C.setElement(i, j, A.getElement(i, j));
			for (int j = 0; j < widthB; j ++)
				C.setElement(i, j + widthA, B.getElement(i, j));
		}
		return C.setImmutable();
	}

	public static final Matrix applyVertical(Matrix A, Matrix B) {
		int width = A.getWidth();
		if (width != B.getWidth())
			throw EXCEPTION_MATRIX_DIMENSION;
		int heightA = A.getHeight();
		int heightB = B.getHeight();
		Matrix C = new Matrix(heightA + heightB, width);
		for (int j = 0; j < width; j ++) {
			for (int i = 0; i < heightA; i ++)
				C.setElement(i, j, A.getElement(i, j));
			for (int i = 0; i < heightB; i ++)
				C.setElement(i + heightA, j, B.getElement(i, j));
		}
		return C.setImmutable();
	}
}