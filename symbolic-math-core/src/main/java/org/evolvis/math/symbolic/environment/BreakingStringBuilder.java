package org.evolvis.math.symbolic.environment;


class BreakingStringBuilder {

	private final StringBuilder sb; // StringBuilder is final so this class cannot extend it and uses a delegating instance
	private int indent;
	private int length = 0;
	public static final int MAX_LINE_LENGTH = 80;
	public static final String INDENT = "    ";
	public static final String LINE_BREAK = "\n";
	
	public BreakingStringBuilder(StringBuilder sb, int indent) {
		this.sb = sb;
		this.indent = indent;
	}
	
	public BreakingStringBuilder append(char ch) {
		return append(Character.toString(ch));
	}
	
	public BreakingStringBuilder append(int i) {
		return append(Integer.toString(i));
	}
	
	public BreakingStringBuilder append(String str) {
		if (length + str.length() > MAX_LINE_LENGTH) {
			appendBreak();
			indent(2);
		}
		sb.append(str);
		length += str.length();
		return this;
	}
	
	public BreakingStringBuilder appendNoWrap(String str) {
		sb.append(str);
		length += str.length();
		return this;
	}
	
	public BreakingStringBuilder appendBreak() {
		if (length > 0)
			forceBreak();
		return this;
	}
	
	public BreakingStringBuilder forceBreak() {
		sb.append(LINE_BREAK);
		length = 0;
		return this;
	}
	
	public void incIndent(int i) {
		indent += i;
	}
	
	public BreakingStringBuilder indent() {
		return indent(0);
	}
	
	public BreakingStringBuilder indent(int indent) {
		indent += this.indent;
		for (int i = indent; i > 0; i--)
			sb.append(INDENT);
		length += indent * INDENT.length();
		return this;
	}
	
	public BreakingStringBuilder newLine() {
		return newLine(0);
	}
	
	public void clear() {
		sb.setLength(0);
	}
	
	public BreakingStringBuilder newLine(int incIndent) {
		appendBreak();
		incIndent(incIndent);
		indent();
		return this;
	}

	@Override
	public String toString() {
		return sb.toString();
	}
}
