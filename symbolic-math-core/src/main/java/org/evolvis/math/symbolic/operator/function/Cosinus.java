package org.evolvis.math.symbolic.operator.function;

import org.evolvis.math.symbolic.Function;
import org.evolvis.math.symbolic.FunctionType;
import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Scalar;

/**
 * singleton
 * 
 * @author Hendrik Helwich
 *
 */
public class Cosinus extends FunctionType {
	
	private static final String ID = "cos";
	
	private static FunctionType instance;

	public Cosinus() {
		instance = this;
	}
	
	@Override
	public String getName() {
		return ID;
	}
	
	@Override
	public int getInputCount() {
		return 1;
	}

	/**
	 * The input list does get 2 elements.
	 * Both parameters must be of type {@link Scalar} or both must be from type {@link Matrix}.
	 * 
	 * @see org.evolvis.math.symbolic.FunctionType#apply(java.util.List)
	 */
	@Override
	public Operand apply(Operand... input) {
		if (input[0] instanceof Scalar)
			return apply((Scalar) input[0]);
		else
			throw EXCEPTION_WRONG_INPUT_TYPE;
	}
	
	public static final Scalar apply(Scalar s) {
		return new Function(instance, s);
	}
	
	@Override
	public Scalar getDerivate(String name, Scalar[] inputs) {
		// (x cos) --> x sin ~ (x) *
		Scalar x = inputs[0];
		Scalar xd  = x.getDerivate(name);
		return Sinus.apply(x).negate().multiply(xd);
	}
}
