package org.evolvis.math.symbolic.traverse;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Scalar;

public abstract class OperandCostCalculator {

	protected abstract double getCost(Scalar scalar);

	protected double getCost(Matrix matrix) {
		int height = matrix.getHeight();
		int width = matrix.getWidth();
		int size = height*width;
		double retc = 0;
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++) {
				double c = getCost(matrix.getElement(i, j));
				c /= size;
				retc += c;
			}
		return retc;
	}
	
	public double getCost(Operand operand) {
		if (operand instanceof Scalar)
			return getCost((Scalar) operand);
		return getCost((Matrix) operand);
	}
}
