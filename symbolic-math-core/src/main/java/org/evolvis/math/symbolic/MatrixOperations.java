package org.evolvis.math.symbolic;

import org.evolvis.math.symbolic.operator.function.Addition;
import org.evolvis.math.symbolic.operator.function.Inversion;
import org.evolvis.math.symbolic.operator.function.Multiplication;
import org.evolvis.math.symbolic.operator.function.condition.EqualTo;
import org.evolvis.math.symbolic.operator.function.condition.IfElse;

public final class MatrixOperations {

	private MatrixOperations() {}
	
	public static Matrix invert(Matrix matrix) {
		if (!matrix.isSquare())
			throw new RuntimeException("must be a square matrix");
		int size = matrix.getHeight();
		Matrix ret = new Matrix(size, size * 2);
		// copy input matrix to the left side
		for (int i=0; i<size; i++)
			for (int j=0; j<size; j++)
				ret.setElement(i, j, matrix.getElement(i, j));
		// create ones at the right side
		for (int i=0; i<size; i++)
			for (int j=0; j<size; j++)
				ret.setElement(i, j+size, i == j ? Number.ONE : Number.ZERO);
		ret.setImmutable();
		// solve
		ret = gaussJordan(ret);
		// return right side
		ret = ret.getSubMatrix(0, size, size, size);
		ret = ret.mergeNumbers();
		return ret;
	}
	

	/**
	 * not initialized
	 * 
	 * @return
	 */
	private static final Scalar[][] shallowCopyMatrix(Matrix matrix) {
		int width  = matrix.getWidth();
		int height = matrix.getHeight();
		Scalar[][] m2 = new Scalar[height][width];
		for (int i = 0; i < height; i ++)
			for (int j = 0; j < width; j ++)
				m2[i][j] = matrix.getElement(i, j);
		return m2;
	}
	
	public static Matrix gaussJordan(Matrix matrix) {
		int width  = matrix.getWidth();
		int height = matrix.getHeight();
		if (height > width)
			throw new RuntimeException("matrix height must not be greater than matrix width");

		// copy matrix array
		Scalar[][] m2 = shallowCopyMatrix(matrix);
		
		for (int i = 0; i < height; i ++) { // iterate over columns and rows
			// find first row starting with row i which is not zero in column j
			for (int k = i+1; k < height; k ++) { // must the current row be swapped?
				// if test element i,i is zero swap row i with row k
				Scalar test = EqualTo.apply(m2[i][i], Number.ZERO);
				for (int j = 0; j < width; j ++) {
					Scalar tmp = IfElse.apply(test, m2[k][j], m2[i][j]);
					m2[k][j] = IfElse.apply(test, m2[i][j], m2[k][j]);
					m2[i][j] = tmp;
				}
			}
			
			Scalar inv = m2[i][i];
			inv = Inversion.apply(inv);
			// multiply row i with inverse element
			for (int k = 0; k < width; k ++) {
				if (k == i) // only to reduce computation time. same as: if (false)
					m2[i][k] = Number.ONE;
				else {
					Scalar s = m2[i][k];
					s = Multiplication.apply(inv, s);
					s = s.mergeNumbers();
					m2[i][k] = s;
				}
			}
			//
			for (int k = i+1; k < height; k ++) { // iterate over rows below the current row
				Scalar factor = m2[k][i];
				factor = Multiplication.apply(Number.ONE_NEG, factor);
				m2[k][i] = Number.ZERO;
				for (int l = i+1; l < width; l ++) { // iterate over columns right to the current column
					Scalar mult = Multiplication.apply(factor, m2[i][l]);
					mult = Addition.apply(mult, m2[k][l]);
					mult = mult.mergeNumbers();
					m2[k][l] = mult;
				}
			}
		}
		// now there are only ones on the diagonal and zeros below the diagonal
		// => eleminate all values above the diagonal
		for (int i = height-1; i >= 0; i --) { // iterate over rows
			for (int k = i-1; k >= 0; k --) { // iterate over rows above the current row
				Scalar factor = m2[k][i];
				factor = Multiplication.apply(Number.ONE_NEG, factor);
				m2[k][i] = Number.ZERO;
				for (int l = i+1; l < width; l ++) { // iterate over columns right to the current column
					Scalar mult = Multiplication.apply(factor, m2[i][l]);
					mult = Addition.apply(mult, m2[k][l]);
					mult = mult.mergeNumbers();
					m2[k][l] = mult;
				}
			}
		}
		return new Matrix(m2);
	}
	
	public static Matrix decomposeUD(Matrix matrix) {
		// copy matrix array
		int n = matrix.getHeight();
		if (n != matrix.getWidth())
			throw FunctionType.EXCEPTION_MATRIX_DIMENSION;
		Scalar[][] M = shallowCopyMatrix(matrix);
		// elements below the diagonal of M will not be used
		for (int j = n-1; j >= 0; j --)
			for (int i = j; i >= 0; i --) {
				Scalar s = M[i][j];
				for (int k = j+1; k < n; k ++)
					s = s.subtract(M[i][k].multiply(M[k][k]).multiply(M[j][k]));
				if (i == j)
					M[j][j] = s;
				else {
					M[j][j] = M[j][j].mergeNumbers();
					M[i][j] = s.divide(M[j][j]);
				}
			}
		// set elements below the diagonal to zero
		for (int i = 1; i < n; i ++)
			for (int j = 0; j < i; j ++)
				M[i][j] = Number.ZERO;
		return new Matrix(M);
	}
	
	public static Matrix forwardSubstitution(Matrix U, Matrix Y) {
		int n = U.getHeight();
		int p = Y.getWidth();
		if (n != U.getWidth() || n != Y.getHeight())
			throw FunctionType.EXCEPTION_MATRIX_DIMENSION;
		Scalar[][] X = new Scalar[n][p];
		for (int j = 0; j < p; j ++)
			for (int i = 0; i < n; i ++) {
				X[i][j] = Y.getElement(i, j);
				for (int k = 0; k < i; k ++)
					X[i][j] = X[i][j].subtract(U.getElement(k, i).multiply(X[k][j]));
			}
		return new Matrix(X);
	}
	
	public static Matrix backwardSubstitution(Matrix U, Matrix Y) {
		int n = U.getHeight();
		int p = Y.getWidth();
		if (n != U.getWidth() || n != Y.getHeight())
			throw FunctionType.EXCEPTION_MATRIX_DIMENSION;
		Scalar[][] X = new Scalar[n][p];
		for (int j = 0; j < p; j ++)
			for (int i = n-1; i >= 0; i --) {
				X[i][j] = Y.getElement(i, j);
				for (int k = i+1; k < n; k ++)
					X[i][j] = X[i][j].subtract(U.getElement(i, k).multiply(X[k][j]));
			}
		return new Matrix(X);
	}
	
	public static Matrix solveDiagonal(Matrix D, Matrix Y) {
		int m = D.getHeight();
		int n = Y.getWidth();
		if (m != D.getWidth() || m != Y.getHeight())
			throw FunctionType.EXCEPTION_MATRIX_DIMENSION;
		Scalar[][] X = new Scalar[m][n];
		for (int i = 0; i < m; i ++)
			for (int j = 0; j < n; j ++)
				X[i][j] = Y.getElement(i, j).divide(D.getElement(i, i));
		return new Matrix(X);
	}
	
	public static Matrix solveSPD(Matrix M, Matrix Y) {
		Matrix UD = decomposeUD(M);
		Matrix Y2 = backwardSubstitution(UD, Y);
		Matrix Y3 = solveDiagonal(UD, Y2);
		return forwardSubstitution(UD, Y3);
	}
	
	public static Matrix invertSPD(Matrix M) {
		Matrix UD = decomposeUD(M);
		// invert upper diagonal matrix U
		int n = UD.getHeight();
		Scalar[][] U = shallowCopyMatrix(UD);
		for (int i = n-2; i >= 0; i --)
			for (int j = n-1; j > i; j --) {
				U[i][j] = U[i][j].negate();
				for (int k = i+1; k < j; k ++)
					U[i][j] = U[i][j].subtract(U[i][k].multiply(U[k][j]));
			}
		// invert diagonal matrix D
		for (int i = 0; i < n; i ++)
			U[i][i] = U[i][i].invert();
		// invert all: U^-1, D^-1  -->  (U^-t * D^-1) * U^-1 = M^-1
		for (int i = n-1; i >= 0; i --) {
			for (int j = 0; j < i; j ++)
				U[i][j] = U[j][i].multiply(U[j][j]);
			for (int j = n-1; j >= i; j --) {
				if (i < j)
					U[i][j] = U[i][j].multiply(U[i][i]);
				for (int k = 0; k < i; k ++)
					U[i][j] = U[i][j].add(U[k][j].multiply(U[i][k]));
				U[j][i] = U[i][j];
			}
		}
		return new Matrix(U);
	}
}
