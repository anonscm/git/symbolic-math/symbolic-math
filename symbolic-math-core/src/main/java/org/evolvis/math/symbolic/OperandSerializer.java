package org.evolvis.math.symbolic;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.evolvis.math.symbolic.environment.OperandEnvironment;
import org.evolvis.util.Stack;



/**
 * This class bundles all serialization and deserialization of the subclasses
 * of the class {@link Operand}. Each of this classes must have a constructor
 * which takes a single string and initializes the instance by calling a
 * deserialization method in this class.
 * The method {@link Operand#toString()} calls the method
 * {@link #toString(Operand)} to serialize the instance to a string.
 * If this {@link String} is used to create the {@link Operand} again by the
 * described constructor, it must always cause an instance which is equal to
 * previous instance (checked via the {@link Operand#equals(Object)} method).
 * {@link Scalar}s can be composed by <code>apply()</code> methods of
 * subclasses of {@link FunctionType}. This {@link Scalar}s can be set as
 * elements of a {@link Matrix} by the
 * {@link Matrix#setElement(int, int, Scalar)} method.
 * As an alternative these composed structures can also be serialized and
 * deserialized by {@link OperandSerializer}. TODO welche Methoden genau?
 * 
 * @author Hendrik Helwich
 */
public class OperandSerializer {
	

	/**
	 * Used when serializing not used when deserializing.
	 */
	public static final char CHAR_NODE_SEPARATOR = ' ';
	
	// Matrix special chars
	
	public static final char CHAR_MATRIX_START   = '(';
	public static final char CHAR_MATRIX_END     = ')';
	public static final char CHAR_ROW_SEPARATOR  = ';';
	public static final char CHAR_CELL_SEPARATOR = ',';
	
	// Number special chars
	
	public static final char CHAR_NUMBER_SEPARATOR_DECIMAL  = '.';
	public static final char CHAR_NUMBER_SEPARATOR_FRACTION = '/';

	// escape special chars
	
	private static final String PATTERN_NUMBER_SEPARATOR_DECIMAL   = toPatternChar(CHAR_NUMBER_SEPARATOR_DECIMAL);
	private static final String PATTERN_NUMBER_SEPARATOR_FRACTION  = toPatternChar(CHAR_NUMBER_SEPARATOR_FRACTION);

	// some regular expressions
	
	private static final String PATTERN_VARIABLE_STRING = "[A-Za-z][A-Za-z0-9_]*";
	
	private static final Pattern PATTERN_NUMBER    = Pattern.compile( "(-?[0-9]+)(("
			+ PATTERN_NUMBER_SEPARATOR_DECIMAL
			+ "([0-9]+))|"
			+ PATTERN_NUMBER_SEPARATOR_FRACTION
			+ "((-?[0-9]+)))?");
	
	private static final Pattern PATTERN_VARIABLE    = Pattern.compile(PATTERN_VARIABLE_STRING);

	/**
	 * Escapes a character in a regular expression if it has a special meaning
	 * in the regular expresiion syntax.
	 * 
	 * @param  c
	 *         Character which should be checked and maybe replaced.
	 * @return The input character as a {@link String}, if it does not has a
	 *         special meaning in the regular expresiion syntax, or an escaped
	 *         form of the input character.
	 */
	private static String toPatternChar(char c) {
		switch (c) {
		case '(':
			return "\\(";
		case ')':
			return "\\)";
		case '.':
			return "\\.";
		default:
			return Character.toString(c);
		}
	}
	

	// ----------------------------------------------------------- Serialization

	// Serialization forwarding
	
	static String toString(Operand op) {
		if (op instanceof Scalar)
			return toString((Scalar) op);
		return toString((Matrix) op);
	}

	private static String toString(Scalar s) {
		if (s instanceof Number)
			return toString((Number) s); 
		else if (s instanceof Variable)
			return toString((Variable) s); 
		return toString((Function) s); 
	}

	// Number serialization
	
	/**
	 * @return
	 * @throws ArithmeticException
	 */
	private static String toDecimalString(Number number) {
		BigDecimal n = new BigDecimal(number.getNominator());
		BigDecimal d = new BigDecimal(number.getDenominator());
		return n.divide(d).toPlainString();
	}
	
	private static String toFractionalString(Number number) {
		return number.getNominator().toString()+CHAR_NUMBER_SEPARATOR_FRACTION+number.getDenominator().toString();
	}

	/**
	 * This method should only be used by {@link Number#toString()}
	 * 
	 * @param number
	 * @return
	 */
	private static String toString(Number number) { //TODO jetzt variante 1: kürzeste Darstellung oder Bruch wenn dezimal nicht möglich; variante 2:  bruchdarstellung nur wenn dezimaldarstellung nicht möglich
		if (Number.ONE.equals(number.getDenominator()))
			return number.getNominator().toString();
		if (Number.NAN.equals(number))
			return "undefined";
		String fs = toFractionalString(number);
		try {
			String ds = toDecimalString(number);
			return ds.length() <= fs.length() ? ds : fs;
		} catch (ArithmeticException e) {
			return fs;
		}
	}
	
	// Variable serialization
	
	private static String toString(Variable variable) {
		return variable.getName();
	}
	
	// Function serialization
	
	private static String toString(Function function) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < function.getInputCount(); i++) {
			sb.append(toString(function.getInput(i)));
			sb.append(CHAR_NODE_SEPARATOR);
		}
		String fname = function.getType().getName();
		sb.append(fname);
		return sb.toString();
	}

	// Matrix serialization
	
	private static String toString(Matrix matrix) {
		StringBuilder sb = new StringBuilder();
		sb.append(CHAR_MATRIX_START);
		for (int i = 0; i < matrix.getHeight(); i++) {
			if (i > 0)
				sb.append(CHAR_ROW_SEPARATOR);
			for (int j = 0; j < matrix.getWidth(); j++) {
				if (j > 0)
					sb.append(CHAR_CELL_SEPARATOR);
				sb.append(toString(matrix.getElement(i, j)));
			}
		}
		sb.append(CHAR_MATRIX_END);
		return sb.toString();
	}
	

	// --------------------------------------------------------- Deserialization

	static Operand parseOperand(String str) throws OperandInitializationException {
		return parseOperand(null, str);
	}

	static Operand parseOperand(OperandEnvironment oe, String str) throws OperandInitializationException {
		return parseOperand(oe, 0, str.length(), str);
	}

	
	/**
	 * This method will not be used in a constructor
	 * 
	 * @param str
	 * @return
	 * @throws OperandInitializationException
	 */
	private static Operand parseOperand(OperandEnvironment env, int start, int end, String str) throws OperandInitializationException {
		Stack<Operand> stack = new Stack<Operand>();
		int i=start; //TODO remove i
		main:
		while (i < end) { // do till end of string is reached
			// skip leading whitespaces
			while (Character.isWhitespace(str.charAt(i)))
				if (i < end && ++i == end)
					break main;
			//
			if (str.charAt(i) == CHAR_MATRIX_START) { // operand is a matrix
				// search matrix end
				int j = 1; // number of started matrices
				int mei=i+1; // end index of the matrix
				ofor:
				for (; mei < end; mei++)
					switch (str.charAt(mei)) {
						case CHAR_MATRIX_START:
							j++;
							break;
						case CHAR_MATRIX_END:
							if (--j == 0)
								break ofor;
					}
				if (j > 0)
					throw new RuntimeException("matrix is not closed");
				stack.push(parseMatrix(env, i+1, mei, str));
				i = mei+1;
			} else { // operand is a scalar or function
				int oei=i+1;
				while (oei < end && !Character.isWhitespace(str.charAt(oei)))
					oei++;
				String token = str.substring(i, oei);
				if (PATTERN_NUMBER.matcher(token).matches()) { // if first char is digit => token represents a number
					stack.push(parseNumber(token));
				} else if (Operator.isOperator(token)) { // token represents a function
					Operator.applyOperator(token, stack);
				} else if (PATTERN_VARIABLE.matcher(token).matches()) { // token represents a variable
					if (env != null && env.contains(token)) {
						Operand op = env.get(token);
						stack.push(op);
					} else
						stack.push(parseVariable(token));
				} else
					throw new RuntimeException("invalid syntax: "+token);
				i = oei;
			}
		}
		if (stack.isEmpty())
			throw new RuntimeException("stack is empty");
		Operand ret = stack.pop();
		if (! stack.isEmpty())
			throw new RuntimeException("stack holds unused nodes: (top: "+stack.pop()+" )");
		return ret.tryScalar();
	}

	// Number deserialization

	/**
	 * This method should only be used by {@link Number#Number(String)}.
	 * 
	 * @param  holder
	 * @param  str
	 * @return
	 * @throws OperandInitializationException 
	 */

	static Number parseNumber(String str) throws OperandInitializationException {
		Matcher matcher = PATTERN_NUMBER.matcher(str);
		if (!matcher.matches())
			throw new OperandInitializationException("illegal number string");
		String prefix = matcher.group(1);
		String decSuffix = matcher.group(4);
		String fracSuffix = matcher.group(5);
		BigInteger nominator;
		BigInteger denominator;
		if (decSuffix != null) { // decimal representation
			nominator = new BigInteger(prefix + decSuffix);
			denominator = BigInteger.TEN.pow(decSuffix.length());
		} else if (fracSuffix != null) { // fractional representation
			nominator = new BigInteger(prefix);
			denominator = new BigInteger(fracSuffix);
		} else { // integer
			nominator = new BigInteger(prefix);
			denominator = BigInteger.ONE;
		}
		return Number.create(nominator, denominator);
	}

	// Variable deserialization

	static Variable parseVariable(String str) throws OperandInitializationException {
		Matcher matcher = PATTERN_VARIABLE.matcher(str);
		if (!matcher.matches())
			throw new OperandInitializationException("illegal variable name");
		return new Variable(str);
	}
	
	// Matrix deserialization
	
	static Matrix parseMatrix(String str) throws OperandInitializationException {
		return parseMatrix(null, str);
	}
	
	static Matrix parseMatrix(OperandEnvironment env, String str) throws OperandInitializationException {
		return parseMatrix(env, 0, str.length(), str);
	}
	
	private static Matrix parseMatrix(OperandEnvironment env, int start, int end, String str) throws OperandInitializationException {
		IncrementalMatrixInitializer matrix = new IncrementalMatrixInitializer();
		int csi = start;
		int mdepth = 0;
		while (csi < end) {
			char ch = str.charAt(csi++);
			if (ch == CHAR_MATRIX_START)
				mdepth++;
			else if (ch == CHAR_MATRIX_END) {
				mdepth--;
				assert(mdepth >= 0);
			} else if ((ch == CHAR_CELL_SEPARATOR || ch == CHAR_ROW_SEPARATOR || csi == end) && mdepth == 0) {
				Operand c = parseOperand(env, start, csi == end ? end : csi-1, str).tryScalar();
				if (!(c instanceof Scalar))
					throw new RuntimeException("a matrix can not hold a matrix");
				matrix.addElement((Scalar)c);
				start = csi;
				if (ch == CHAR_ROW_SEPARATOR)
					matrix.endRow();
			}
		}
		return matrix.endMatrix();
	}
}