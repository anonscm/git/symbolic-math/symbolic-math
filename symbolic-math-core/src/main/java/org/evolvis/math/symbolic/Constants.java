package org.evolvis.math.symbolic;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A singleton which holds some general constants which are defined in the
 * properties file {@value #FILENAME}.
 *
 * @author Hendrik Helwich
 */
public final class Constants {

    /**
     * A logger.
     */
    private static final transient Logger logger = Logger.getLogger(Constants.class.getName());

    /**
     * The name of the file which specfies the constants. If the filename is
     * relative it will be loaded in relation to the folder of this class.
     */
    private static final String FILENAME = "constants.txt";

    /**
     * The singleton instance which will be created directly to be thread-safe.
     */
    private static Constants instance = new Constants();

    /**
     * A {@link Map} which holds the constants. The key of the {@link Map} is
     * the name of the constant, the value is the constant.
     */
    private final transient Map <String, Operand> constantMap;
    
    private boolean initialized = false;

    /**
     * Reads all constants which are defined in the file {@value #FILENAME}.
     * If the specified file does not exist or an {@link IOException} appears
     * while reading this file, the stored list of constants is empty and the
     * method {@link #isConstant(String)} will always return <code>false</code>
     * and the method {@link #getConstant(String)} will always return
     * <code>null</code>.
     */
    private Constants() {
        constantMap = new HashMap <String, Operand>();
        final Properties properties = new Properties();
        final InputStream inStream = getClass().getResourceAsStream(FILENAME);
        if (inStream == null) {
            logger.severe("cannot find constant file: " + FILENAME);
        } else {
            try {
                properties.load(inStream);
                for (Map.Entry <Object, Object> entry : properties.entrySet()) {
                    final String name = (String) entry.getKey();
                    final String formula = (String) entry.getValue();
                    Operand nnm = Operand.parse(formula);
                    nnm = nnm.mergeNumbers();
                    constantMap.put(name, nnm);
                }
                initialized = true;
            } catch (IOException e) {
                logger.log(Level.SEVERE,
                        "error reading constant file: " + FILENAME, e);
            } catch (OperandInitializationException e) {
                logger.log(Level.SEVERE,
                        "error parsing file: " + FILENAME, e);
			}
        }
    }

    /**
     * Returns the constant with the given name. If the name is unknown the
     * method returns <code>null</code>.
     *
     * @param  name
     *         The name of the requested constant
     * @return The requested constant or <code>null</code> if a constant with
     *         the specified name is unknwon
     */
    public static Operand getConstant(final String name) {
        return instance.constantMap.get(name);
    }

    /**
     * Checks if a constant with the given name does exist.
     *
     * @param  name
     *         The name of the constant
     * @return <code>true</code> if a constant with the given name does exist
     */
    public static boolean isConstant(final String name) {
        return instance.constantMap.containsKey(name);
    }

	public static boolean isInitialized() {
        return instance.initialized;
	}
}
