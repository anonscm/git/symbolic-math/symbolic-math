package org.evolvis.math.symbolic;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Assures that every subclass is a singleton, because it checks
 * that there are not two instances which return the same value
 * by the operation {@link #getName()}.
 * 
 * 
 * @author Hendrik Helwich
 */
public abstract class FunctionType extends Operator {
	
	private static Map<String, FunctionType> typeMap;
	
	public static enum ParameterType {
		RATIONAL, BOOLEAN
	}
	
	protected FunctionType() {
		if (typeMap == null)
			typeMap = new HashMap<String, FunctionType>();
		validate();
	}

	public abstract String getName();

	@Override
	protected boolean matchToken(String token) {
		if (token == null)
			throw new NullPointerException();
		return token.equals(getName());
	}

	@Override
	protected Operand apply(String token, Operand... input) {
		// it is assured that token.equals(getName())
		return apply(input);
	}

	/**
	 * 
	 * 
	 * @param  input
	 *         It will be assured, that the list does have the size {@link #getInputCount()}.
	 *         The list does not contain {@link Matrix} instances of the dimension 1x1.
	 * @return
	 */
	protected abstract Operand apply(Operand... input);

	private void validate() {
		if (getName() == null)
			throw EXCEPTION_INVALID_OPERATOR;
		if (isFunctionType(getName()))
			throw EXCEPTION_INVALID_OPERATOR;
		typeMap.put(getName(), this);
	}

	public static final Collection<FunctionType> getFunctionTypes() {
		return typeMap.values();
	}
	
	public static final boolean isFunctionType(String name) {
		return typeMap.keySet().contains(name);
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		final String name = getName();
		int result = 1;
		result = PRIME * result + name.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final FunctionType other = (FunctionType) obj;
		return getName().equals(other.getName());
	}

	public abstract Scalar getDerivate(String name, Scalar[] inputs);
	
	public ParameterType getInputType(int idx) {
		return ParameterType.RATIONAL;
	}

	public ParameterType getOutputType() {
		return ParameterType.RATIONAL;
	}
}
