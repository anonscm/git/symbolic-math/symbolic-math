package org.evolvis.math.symbolic.traverse;

public interface TransformerUpdateListener<T> {

	public void newTransformation(T transformation, double cost);
	
}
