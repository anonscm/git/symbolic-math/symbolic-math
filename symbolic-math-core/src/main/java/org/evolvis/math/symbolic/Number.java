package org.evolvis.math.symbolic;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;

/**
 * An immutable arbitrary-precision representation of a rational number.
 *
 * @author Hendrik Helwich
 */
public class Number extends Scalar {

	public static final Number ZERO    = new Number(BigInteger.ZERO, BigInteger.ONE);
	public static final Number ONE     = new Number(BigInteger.ONE,  BigInteger.ONE);
	public static final Number TEN     = new Number(BigInteger.TEN,  BigInteger.ONE);
	public static final Number ONE_NEG = ONE.negate();
	public static final Number TEN_INV = TEN.invert();
	public static final Number NAN     = new Number(BigInteger.ZERO,  BigInteger.ZERO);

	private static final int TO_DOUBLE_SCALE = 20;
	
	private BigInteger nominator;
	private BigInteger denominator;
	
	public static Number create(String str) {
		Scalar s = Scalar.create(str);
		if (!(s instanceof Number))
			throw SERIALIZATION_WRONG_TYPE;
		return (Number) s;
	}
	
	public static Number create(long n) {
		return create(BigInteger.valueOf(n), BigInteger.ONE);
	}
	
	public static Number create(BigInteger nominator, BigInteger denominator) {
		if (denominator.equals(BigInteger.ZERO))
			return NAN;
		return new Number(nominator, denominator);
	}

	/**
	 * Used by this class and by {@link OperandSerializer}.
	 * 
	 * @see #Number()
	 */
	private Number(BigInteger nominator, BigInteger denominator) {
		// simplify
		if (! denominator.equals(BigInteger.ZERO)) {
			BigInteger gcd = nominator.gcd(denominator);
			nominator = nominator.divide(gcd);
			denominator = denominator.divide(gcd);
			// assure that only the nominator can has a negative signum
			if (denominator.signum() == -1) { // multiply fraction with -1/-1
				denominator = denominator.negate();
				nominator = nominator.negate();
			}
		} else if (NAN != null)
			throw new RuntimeException("illegal internal call");
		this.nominator = nominator;
		this.denominator = denominator;
		setImmutable();
	}

	public BigInteger getNominator() {
		return nominator;
	}

	/**
	 * is always a number which is greater than zero
	 * 
	 * @return
	 */
	public BigInteger getDenominator() {
		return denominator;
	}
	
	/**
	 * Returns <code>true</code> if the denominator is 1.
	 * 
	 * @return <code>true</code> if the denominator is 1
	 */
	public boolean isIntegral() {
		return denominator.equals(BigInteger.ONE);
	}

	public Number negate() {
		if (isNaN())
			return NAN;
		return new Number(nominator.negate(), denominator);
	}
	
	/**
	 * Returns a new number which is the multiplicative inverse, so that:
	 * <code>Number.ONE.equals( a.invert().multiply(a) )</code>.
	 * 
	 * @return A new number which is the multiplicative inverse
	 * @throws ArithmeticException
	 *         If the current number is equal to {@link #ZERO}.
	 */
	public Number invert() throws ArithmeticException {
		if (isNaN() || equals(ZERO))
			return NAN;
		return new Number(denominator, nominator);
	}
	
	public Number add(Number number) {
		if (isNaN() || number.isNaN())
			return NAN;
		if (denominator.equals(number.denominator))
			return new Number(nominator.add(number.nominator), denominator);
		else
			return new Number(nominator.multiply(number.denominator)
					.add(number.nominator.multiply(denominator)),
					denominator.multiply(number.denominator));
	}
	
	public Number multiply(Number number) {
		if (isNaN() || number.isNaN())
			return NAN;
		return new Number(nominator.multiply(number.nominator),
				denominator.multiply(number.denominator));
	}
	
	public Number subtract(Number number) {
		return add(number.negate());
	}
	
	public Number divide(Number number) {
		return multiply(number.invert());
	}
	
	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public Scalar selectNode(int idx) {
		return idx == 0 ? this : null;
	}

	@Override
	public Scalar replaceNode(int idx, Scalar scalar) {
		return idx == 0 && !equals(scalar) ? scalar : this;
	}
	
    /**
     *
     * @return
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public final int hashCode() {
        final int PRIME = 31;
        int result = 1;
        result = PRIME * result + denominator.hashCode();
        result = PRIME * result +   nominator.hashCode();
        return result;
    }
    
	/**
     * @param  obj
     *         
     * @return 
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
	@Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Number other = (Number) obj;
        return    other.nominator  .equals(nominator)
               && other.denominator.equals(denominator);
    }
	
	public double toDouble() {
		BigDecimal n = new BigDecimal(nominator);
		BigDecimal d = new BigDecimal(denominator);
		return n.divide(d, TO_DOUBLE_SCALE, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	@Override
	public boolean isAtomic() {
		return true;
	}

	@Override
	public Scalar mergeNumbers() {
		return this;
	}
	
	public boolean isNaN() {
		return this == NAN;
	}

	@Override
	protected void getVariables(Collection<Variable> variables) {}

	@Override
	public Scalar replaceScalar(Scalar find, Scalar replace) {
		return equals(find) ? replace : this;
	}
}
