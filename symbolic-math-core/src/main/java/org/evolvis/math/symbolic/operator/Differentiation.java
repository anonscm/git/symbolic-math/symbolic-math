package org.evolvis.math.symbolic.operator;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Operator;
import org.evolvis.math.symbolic.Scalar;
import org.evolvis.math.symbolic.Variable;

public class Differentiation extends Operator {

	private static final String ID = "jacobi";

	@Override
	protected boolean matchToken(String token) {
		return ID.equals(token);
	}

	@Override
	public int getInputCount() {
		return 2;
	}

	@Override
	protected Operand apply(String token, Operand... input) {
		return apply(input[0].asMatrix(), input[1].asMatrix());
	}
	
	public static final Matrix apply(Matrix functions, Matrix variables) {
		// check if matrices have width 1
		if (functions.getWidth() > 1)
			throw new RuntimeException("matrix must have width 1");
		if (variables.getWidth() > 1)
			throw new RuntimeException("matrix must have width 1");
		// store jacobi dimension
		int height = functions.getHeight(), width = variables.getHeight();
		// store variables in array
		Variable[] var = new Variable[width];
		for (int i = 0; i < width; i ++) {
			Scalar e = variables.getElement(i, 0);
			if (! (e instanceof Variable))
				throw new RuntimeException("matrix element must be a variable");
			var[i] = (Variable) e;
		}
		// create jacobi matrix
		Matrix jacobi = new Matrix(height, width);
		for (int j = 0; j < width; j ++) {
			Variable v = var[j];
			for (int i = 0; i < height; i ++) {
				Scalar s = functions.getElement(i, 0).getDerivate(v.getName());
				s = s.mergeNumbers();
				jacobi.setElement(i, j, s);
			}
		}
		return jacobi.setImmutable();
	}
}
