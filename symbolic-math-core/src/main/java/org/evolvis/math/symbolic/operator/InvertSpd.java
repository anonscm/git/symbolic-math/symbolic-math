package org.evolvis.math.symbolic.operator;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.MatrixOperations;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Operator;

public class InvertSpd extends Operator {

	public static final String ID = "spd^-1";

	@Override
	protected boolean matchToken(String token) {
		return ID.equals(token);
	}

	@Override
	public int getInputCount() {
		return 1;
	}

	@Override
	protected Operand apply(String token, Operand... input) {
		return apply(input[0].asMatrix());
	}

	public static final Matrix apply(Matrix M) {
		return MatrixOperations.invertSPD(M);
	}
}
