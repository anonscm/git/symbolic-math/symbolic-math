package org.evolvis.math.symbolic.traverse;

import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

import org.evolvis.math.symbolic.Scalar;

public class BoundedScalarArray {
	
	private Scalar[] queue;
	private int maxSize;
	private int size = 0;
	private int lastInserted = -1;
	
	public BoundedScalarArray(int maxSize) {
		if (maxSize < 1)
			throw new RuntimeException("maximum size must be greater than zero");
		this.maxSize = maxSize;
		queue = new Scalar[maxSize];
	}
	
	public void add(Scalar s) {
		if (s == null)
			throw new NullPointerException();
		if (size == maxSize) {
			lastInserted ++;
			if (lastInserted >= maxSize)
				lastInserted = 0;
			queue[lastInserted] = s;
		} else { // size < maxSize
			queue[++lastInserted] = s;
			size++;
		}
	}

	public void addAll(Collection<Scalar> sc) {
		Iterator<Scalar> its = sc.iterator();
		while(its.hasNext())
			add(its.next());
	}

	private static final Random random = new Random();
	
	public Scalar getRandom() {
		if (size <= 0)
			throw new RuntimeException("no scalars added");
		return queue[random.nextInt(size)];
	}
	
	public int getSize() {
		return size;
	}
}
