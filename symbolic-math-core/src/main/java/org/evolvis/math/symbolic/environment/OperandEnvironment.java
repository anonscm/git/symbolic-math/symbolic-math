package org.evolvis.math.symbolic.environment;

import java.io.Reader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Scalar;
import org.evolvis.math.symbolic.Variable;

/**
 * Holds {@link Operand}s with a specified name.
 * If a {@link Matrix} has an element of type {@link Variable} and the name of
 * this variable does start with the name of the matrix, the position and the
 * name of this matrix are stored for this variable.
 * This can be used to create methods which has matrices as an argument when
 * generating code form an {@link OperandEnvironment}. 
 * 
 * @author Hendrik Helwich
 */
public class OperandEnvironment {
	
	/**
	 * Holds {@link Operand}s with a specified name.
	 */
	private final Map<String, Operand> env = new LinkedHashMap<String, Operand>(); // LinkedHashMap to keep input order
	
	/**
	 * Holds references of {@link Variable}s to a specified {@link Matrix}.
	 * The keys of this {@link Map} are the {@link Variable} names, the value
	 * is a data structure which holds all positions of this {@link Variable}
	 * in the {@link Matrix}.
	 */
	private final Map<String, VariableMatrixReference> ref = new HashMap<String, VariableMatrixReference>();

	/**
	 * Create an empty environment.
	 */
	public OperandEnvironment() {}

	/**
	 * Create an environment which reads and parses the specified
	 * {@link Reader} and fills the environment
	 * 
	 * @param  reader
	 *         The stream which will be read to fill the environment.
	 */
	public OperandEnvironment(Reader reader) {
		putAll(reader);
	}
	
	/**
	 * Checks if the environment contains an {@link Operand} with the specified
	 * name.
	 *
	 * @param  name
	 *         The name of the {@link Operand} which should be checked.
	 * @return <code>true</code> if an {@link Operand} whith the given name is
	 *         known, <code>false</code> otherwise.
	 */
	public boolean contains(String name) {
		return env.containsKey(name);
	}

	/**
	 * Get the {@link Operand} whith the given name.
	 * 
	 * @param  name
	 *         The name of the {@link Operand} which is needed.
	 * @return The {@link Operand} which is stored under the given name, or
	 *         <code>null</code> if it is unknown to this environment.
	 */
	public Operand get(String name) {
		return env.get(name);
	}

	/**
	 * Get all the names of the {@link Operand}s which are stored in this
	 * environment.
	 * 
	 * @return All the names of the {@link Operand}s which are stored in this
	 *         environment.
	 */
	public Set<String> nameSet() {
		return env.keySet();
	}

	/**
	 * Add the given {@link Operand} under the given name to this environment.
	 * The name must be new to the environment.
	 * If the {@link Operand} is a {@link Matrix} and it holds
	 * {@link Variable}s which name starts whith the given {@link Matrix} name
	 * as a top level element, the position of this {@link Variable}s in the
	 * {@link Matrix} are stored.
	 * If references are stored for a {@link Variable} can be checked by the
	 * method {@link #isReference(Variable)}. The references can be achieved by
	 * the method {@link #getReference(Variable)}.
	 * 
	 * @param  name
	 *         The name under which the given {@link Operand} should be stored
	 *         in this environment.
	 * @param  operand
	 *         The {@link Operand} which should be stored under the given name
	 *         in this environment.
	 * @throws RuntimeException
	 *         If the environment allready containes an {@link Operand} with
	 *         the given name.
	 * @throws NullPointerException
	 *         If one of the arguments is <code>null</code>.
	 */
	public void put(String name, Operand operand) {
		if (name == null || operand == null)
			throw new NullPointerException();
		if (contains(name))
			throw new RuntimeException("key \""+name+"\" does already exist");
		if (operand instanceof Matrix) { // check if the position of a matrix variable should be stored
			Matrix m = (Matrix) operand; // (if the variable name starts with the matrix name
			int width = m.getWidth();
			int height = m.getHeight();
			for (int i = 0; i < height; i ++)
				for (int j = 0; j < width; j ++) {
					Scalar s = m.getElement(i, j);
					if (s instanceof Variable) {
						String vname = ((Variable) s).getName();
						if (vname.startsWith(name)) // store reference to matrix
							addMatrixReference(vname, name, i, j);
					}
				}
		}
		env.put(name, operand);
	}
	
	/**
	 * Reads and parses all the assignments in this stream and add them to this
	 * environment.
	 * 
	 * @param  reader
	 *         The stream which holds the assignments which should be added to
	 *         this environment.
	 */
	public void putAll(Reader reader) {
		OperandEnvironmentSerializer.parse(this, reader);
	}
	
	/**
	 * Used by {@link #put(String, Operand)}.
	 * 
	 * @param varName
	 * @param matrixName
	 * @param row
	 * @param column
	 * 
	 * @throws RuntimeException
	 *         If a reference for a {@link Variable} with the given name does
	 *         exist for a {@link Matrix} with an other name as the one which
	 *         is specified.
	 */
	private void addMatrixReference(String varName, String matrixName, int row, int column) {
		assert varName != null;    // assured by #put(String, Operand)
		assert matrixName != null; // s.a.
		VariableMatrixReference r = ref.get(varName);
		if (r != null) { // reference with the same name does exist
			if (! r.getMatrixName().equals(matrixName))
				throw new RuntimeException("the variable \""+varName+"\" is already used for matrix \""+r.getMatrixName()+"\"");
			r.addMatrixIndex(row, column);
		} else { // no reference with this name does exist
			VariableMatrixReference vmref = new VariableMatrixReference(matrixName);
			vmref.addMatrixIndex(row, column);
			ref.put(varName, vmref);
		}
	}
	
	/**
	 * Get all the names of the {@link Variable}s which reference to a
	 * {@link Matrix} in this environment.
	 * 
	 * @return All the names of the {@link Variable}s which reference to a
	 *         {@link Matrix} in this environment.
	 */
	public Set<String> referenceSet() {
		return ref.keySet();
	}
	
	/**
	 * Check if a reference to a {@link Matrix} position for the given
	 * {@link Variable} is created while populating the environment with the
	 * operations{@link #put(String, Operand)} and {@link #putAll(Reader)}.
	 * 
	 * @param  vname
	 *         The name of the {@link Variable} for which it should be checked
	 *         if a reference to a {@link Matrix} position does exist.
	 * @return <code>true</code> if a reference to a {@link Matrix} position
	 *         does exist and <code>false</code> otherwise.
	 */
	public boolean isReference(String vname) {
		return ref.containsKey(vname);
	}
	
	/**
	 * Get a reference to all the positions to a {@link Matrix} for the given
	 * {@link Variable} which are stored while populating the environment with
	 * the operations {@link #put(String, Operand)} and {@link #putAll(Reader)}.
	 * 
	 * @param  variable
	 *         The {@link Variable} for which a reference to the positions of a
	 *         {@link Matrix} should be returned.
	 * @return A reference to all the positions to a {@link Matrix} for the given
	 *         {@link Variable} or <code>null</code> if no reference is stored.
	 */
	//TODO javadoc param
	public VariableMatrixReference getReference(String name) {
		return ref.get(name);
	}
}