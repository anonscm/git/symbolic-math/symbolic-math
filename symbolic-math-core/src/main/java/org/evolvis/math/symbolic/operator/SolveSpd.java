package org.evolvis.math.symbolic.operator;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.MatrixOperations;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Operator;

public class SolveSpd extends Operator {

	public static final String ID = "slvspd";

	@Override
	protected boolean matchToken(String token) {
		return ID.equals(token);
	}

	@Override
	public int getInputCount() {
		return 2;
	}

	@Override
	protected Operand apply(String token, Operand... input) {
		return apply(input[0].asMatrix(), input[1].asMatrix());
	}

	public static final Matrix apply(Matrix A, Matrix B) {
		return MatrixOperations.solveSPD(A, B);
	}
}
