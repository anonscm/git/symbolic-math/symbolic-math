package org.evolvis.math.symbolic.operator.function.condition;

import org.evolvis.math.symbolic.Function;
import org.evolvis.math.symbolic.FunctionType;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Scalar;

/**
 * singleton
 * 
 * @author Hendrik Helwich
 */
public class IfElse extends FunctionType {
	
	private static final String ID = "if";
	
	private static FunctionType instance;

	public IfElse() {
		instance = this;
	}
	
	@Override
	public String getName() {
		return ID;
	}
	
	@Override
	public int getInputCount() {
		return 3;
	}

	@Override
	public Operand apply(Operand... input) {
		if (input[0] instanceof Scalar && input[1] instanceof Scalar)
			return apply((Scalar) input[0], (Scalar) input[1], (Scalar) input[2]);
		else
			throw EXCEPTION_WRONG_INPUT_TYPE;
	}
	
	public static final Scalar apply(Scalar condition, Scalar sTrue, Scalar sFalse) {
//		if (sTrue instanceof Exception && sFalse instanceof Exception)
//			throw new RuntimeException("not both cases of an if-else-statement can throw an exception");
		return new Function(instance, condition, sTrue, sFalse);
	}

	@Override
	public Scalar getDerivate(String name, Scalar[] inputs) {
		// (c t f if)  -->  c (t) (f) if
		Scalar st  = inputs[1].getDerivate(name);
		Scalar sf = inputs[2].getDerivate(name);
		return apply(inputs[0], st, sf);
	}

	@Override
	public ParameterType getInputType(int idx) {
		return idx == 0 ? ParameterType.BOOLEAN : ParameterType.RATIONAL;
	}
}
