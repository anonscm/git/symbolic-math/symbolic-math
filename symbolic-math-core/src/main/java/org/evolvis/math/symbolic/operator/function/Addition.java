package org.evolvis.math.symbolic.operator.function;

import org.evolvis.math.symbolic.Function;
import org.evolvis.math.symbolic.FunctionType;
import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Scalar;

/**
 * singleton
 * 
 * @author Hendrik Helwich
 *
 */
public class Addition extends FunctionType {
	
	private static final String ID = "+";
	
	private static FunctionType instance;

	public Addition() {
		instance = this;
	}
	
	@Override
	public String getName() {
		return ID;
	}
	
	@Override
	public int getInputCount() {
		return 2;
	}

	/**
	 * The input list does get 2 elements.
	 * Both parameters must be of type {@link Scalar} or both must be from type {@link Matrix}.
	 * 
	 * @see org.evolvis.math.symbolic.FunctionType#apply(java.util.List)
	 */
	@Override
	public Operand apply(Operand... input) {
		if (input[0] instanceof Scalar) {
			if (input[1] instanceof Scalar) // standard addition
				return apply((Scalar) input[0], (Scalar) input[1]);
			else
				throw EXCEPTION_WRONG_INPUT_TYPE;
		} else {
			if (input[1] instanceof Scalar)
				throw EXCEPTION_WRONG_INPUT_TYPE;
			else // matrix addition
				return apply((Matrix) input[0], (Matrix) input[1]);
		}
	}
	
	public static final Scalar apply(Scalar s1, Scalar s2) {
		return new Function(instance, s1, s2);
	}
	
	/**
	 * Creates a new instance of {@link Matrix}.
	 * Apply function {@link #apply(Scalar, Scalar)} on every element of the
	 * matrices.
	 * 
	 * @param  matrix1
	 * @param  matrix2
	 * @return
	 */
	public static final Matrix apply(Matrix matrix1, Matrix matrix2) {
		if (! matrix1.isSameSize(matrix2))
			throw EXCEPTION_MATRIX_DIMENSION;
		Matrix matrix3 = new Matrix(matrix1.getHeight(), matrix1.getWidth());
		for (int i = 0; i < matrix1.getHeight(); i++)
			for (int j = 0; j < matrix1.getWidth(); j++)
				matrix3.setElement(i, j, 
					apply(matrix1.getElement(i, j), matrix2.getElement(i, j)));
		return matrix3.setImmutable();
	}

	@Override
	public Scalar getDerivate(String name, Scalar[] inputs) {
		// (x y +) --> (x) (y) +
		Scalar xd  = inputs[0].getDerivate(name);
		Scalar yd = inputs[1].getDerivate(name);
		return apply(xd, yd);
	}
}
