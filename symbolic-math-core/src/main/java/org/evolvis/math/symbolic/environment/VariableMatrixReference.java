package org.evolvis.math.symbolic.environment;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Variable;

/**
 * A reference of a {@link Variable} which is an element of a {@link Matrix}.
 * All positions of this {@link Variable} as a top level element of a
 * {@link Matrix} are stored.
 * This is used in {@link OperandEnvironment} to store all needed references
 * to enable the generation of code with {@link CodeGenerator} which allows
 * matrices as operation parameters.
 * 
 * @author Hendrik Helwich
 */
class VariableMatrixReference {

	/**
	 * The name of the {@link Matrix} which is stored in
	 * {@link OperandEnvironment}.
	 */
	private final String matrixName;
	
	/**
	 * All positions of the {@link Variable} in the {@link Matrix} are stored
	 * here in a sorted {@link List}.
	 * The {@link List} must not hold duplicates which is assured by the method
	 * {@link #addMatrixIndex(int, int)}. 
	 */
	private final List<MatrixIndex> pos = new LinkedList<MatrixIndex>();
	
	public List<MatrixIndex> getMatrixIndices() {
		return pos;
	}

	public VariableMatrixReference(String matrixName) {
		if (matrixName == null)
			throw new NullPointerException();
		this.matrixName = matrixName;
	}
	
	public void addMatrixIndex(int row, int column) {
		MatrixIndex mp = new MatrixIndex(row, column);
		int idx = Collections.binarySearch(pos, mp);
		if (idx < 0) // item is not an element of the list => add
	    	pos.add(-idx-1, mp);
	}
	
	public String getMatrixName() {
		return matrixName;
	}

	@Override
	public String toString() {
		return matrixName + pos.toString();
	}
}
