package org.evolvis.math.symbolic.traverse;

import java.util.LinkedList;
import java.util.List;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Scalar;

public class MatrixTransformer implements Runnable {
	
	private Thread thread;
	private ScalarTransformer[][] stm;
	private int width, height;
	private OperandCostCalculator scc;
	private double bestMatrixCost;
	private Matrix bestMatrix;

	public MatrixTransformer() {
		this(new CalculationErrorCostCalculator());
	}

	public MatrixTransformer(OperandCostCalculator scc) {
		this.scc = scc;
	}
	
	private List<TransformerUpdateListener<Matrix>> updateListener;

	public void addUpdateListener(TransformerUpdateListener<Matrix> listener) {
		if (listener != null) {
			if (updateListener == null)
				updateListener = new LinkedList<TransformerUpdateListener<Matrix>>();
			updateListener.add(listener);
		}
	}
	
	private void notifyUpdateListener() {
		if (updateListener != null)
			for (TransformerUpdateListener<Matrix> listener : updateListener)
				listener.newTransformation(bestMatrix, bestMatrixCost);
	}
	
	public synchronized void start(Matrix matrix) {
		start(matrix, true);
	}
	
	private static final int MAX_THREAD_COUNT = 50;
	
	public synchronized void start(Matrix matrix, boolean checkMinimal) {
		if (matrix == null)
			throw new NullPointerException();
		if (thread != null)
			throw new RuntimeException("thread is already running");
		width = matrix.getWidth();
		height = matrix.getHeight();
		stm = new ScalarTransformer[height][width];
		bestMatrix = matrix.mergeNumbers();
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++) {
				Scalar s = bestMatrix.getElement(i, j);
				ScalarTransformer str = new ScalarTransformer(s, checkMinimal, scc);
				str.addUpdateListener(new ElementUpdateListener(i, j));
				stm[i][j] = str;
			}
		bestMatrixCost = scc.getCost(bestMatrix);
		thread = new Thread(this);
		thread.start();
	}
	
	private final synchronized void updateElement(int row, int column, Scalar transformation, double cost) {
		Matrix ret = new Matrix(height, width);
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++) {
				if (i == row && j == column)
					ret.setElement(i, j, transformation);
				else
					ret.setElement(i, j, bestMatrix.getElement(i, j));
			}
		ret = ret.setImmutable();
		double retc = scc.getCost(ret);
		if (retc < bestMatrixCost) {
			bestMatrix = ret;
			bestMatrixCost = retc;
			notifyUpdateListener();
		}
	}
	
	public void stop() {
		if (isRunning())
			for (int i = 0; i < height; i++)
				for (int j = 0; j < width; j++)
					if (stm[i][j].isRunning()) {
						stm[i][j].stop();
					}
	}
	
	public void wait(Matrix matrix, long time) {
		start(matrix);
		long startTime = System.currentTimeMillis();
		while (isRunning() && time > System.currentTimeMillis() - startTime) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public boolean isRunning() {
		return thread != null;
	}

	public void run() {
		try {
			int i=0; // index to next not started scalar thread
			int[] ati = new int[MAX_THREAD_COUNT]; // Matrix index of all active threads
			for (int k = 0; k < MAX_THREAD_COUNT; k ++)
				ati[k] = -1; // set undefined
			
			for (boolean running = false; i < height * width || running;) {
				running = false;
				for (int k = 0; k < MAX_THREAD_COUNT; k ++)
					if (ati[k] == -1) {
						if (i < height * width) {
							stm[i / width][i % width].start();
							ati[k] = i;
							i++;
							running = true;
						}
					} else if (! stm[ati[k] / width][ati[k] % width].isRunning()) { // open slot
						if (i < height * width) {
							stm[i / width][i % width].start();
							ati[k] = i;
							i++;
							running = true;
						}
					} else
						running = true;
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} finally {
			thread = null;
		}
	}
	
	public Matrix getBest() {
		return bestMatrix;
	}

	private class ElementUpdateListener implements TransformerUpdateListener<Scalar> {

		private final int row;
		private final int column;
		
		public ElementUpdateListener(int row, int column) {
			this.row = row;
			this.column = column;
		}
		
		public void newTransformation(Scalar transformation, double cost) {
			updateElement(row, column, transformation, cost);
		}
	}
}
