package org.evolvis.math.symbolic;

import java.util.Collection;

import org.evolvis.math.symbolic.operator.Transpose;
import org.evolvis.math.symbolic.operator.function.Addition;
import org.evolvis.math.symbolic.operator.function.Multiplication;
import org.evolvis.math.symbolic.operator.function.Subtraction;



/**
 * An immutable matrix object.
 * 
 * @author Hendrik Helwich
 */
public class Matrix extends Operand {
	
	private int height;
	private int width;
	
	/**
	 * Should not be accessed directly if possible, to check if writing/reading is allowed
	 */
	private Scalar[][] matrix;

	/**
	 * Afterwards the matrix must be filled with the matrix elements via the
	 * {@link #setElement(int, int, Scalar)} or {@link #setRow(int, Scalar[])}
	 * methods and the method {@link #isInitialized()} must be called by the
	 * constructing method to mark this matrix as an immutable object.
	 * 
	 * @param  height
	 * @param  width
	 */
	public Matrix(int height, int width) {
		this.height = height;
		this.width = width;
		matrix = new Scalar[height][width];
	}
	
	/**
	 * The matrix is marked as initialized afterwards. So the method
	 * {@link #isInitialized()} will always return <code>true</code>.
	 * 
	 * @param element
	 */
	public Matrix(Scalar element) {
		this(1, 1);
		setElement(0, 0, element);
		setImmutable();
	}
	
	public static Matrix create(String str) {
		return OperandSerializer.parseOperand(str).asMatrix();
	}
	
	/**
	 * This operation should only be used in the contruction phase of a new
	 * {@link Matrix}. This assures that a matrix is immutable afterwards.
	 * 
	 * @param  row
	 * @param  rowElements
	 */
	public void setRow(int row, Scalar... rowElements) {
		if (rowElements.length != width)
			throw new RuntimeException("wrong dimemsion");
		for (int i = 0; i < width; i++)
			setElement(row, i, rowElements[i]);
	}

	public Matrix(Scalar[][] elements) {
		this(elements.length, elements[0].length);
		if (elements.length != height)
			throw new RuntimeException("wrong dimemsion");
		for (int i = 0; i < height; i++) {
			if (elements[i].length != width)
				throw new RuntimeException("wrong dimemsion");
			for (int j = 0; j < width; j++)
				setElement(i, j, elements[i][j]);
		}
		setImmutable();
	}
	
	/**
	 * This operation should only be used in the contruction phase of a new
	 * {@link Matrix}. This assures that a matrix is immutable afterwards.
	 * 
	 * @param  row
	 * @param  column
	 * @param  element
	 */
	public void setElement(int row, int column, Scalar element) {
		if (isImmutable)
			throw IMMUTABLE_IS_READONLY;
		if (! element.isImmutable)
			throw IMMUTABLE_EXPECTED;
		matrix[row][column] = element;
	}
	
	public Scalar getElement(int row, int column) {
		return matrix[row][column];
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}
	
	public boolean isScalar() {
		return height == 1 && width == 1;
	}
	
	public boolean isSameSize(Matrix matrix) {
		return height == matrix.height && width == matrix.width;
	}
	
	public boolean isSquare() {
		return height == width;
	}
	
	@Override
	public Matrix setImmutable() {
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				if (matrix[i][j] == null) // access directly because matrix is not initialized yet
					throw new RuntimeException("Matrix containes a null element");
		return (Matrix) super.setImmutable();
	}
	
	public Matrix getSubMatrix(int i, int j, int height, int width) {
		if (i < 0 || j < 0 || height < 1 || width < 1
				|| i + height > this.height || j + width > this.width)
			throw new RuntimeException("out of bounce");
		Matrix m = new Matrix(height, width);
		for (int k = 0; k < height; k ++)
			for (int l = 0; l < width; l ++)
				m.setElement(k, l, getElement(k + i, l + j));
		return m.setImmutable();
	}

	@Override
	public int hashCode() {
		if (!isImmutable)
			throw IMMUTABLE_EXPECTED;
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + height;
		result = PRIME * result + width;
		// can't use Arrays.hashCode() because it does not work for dimension > 1
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
	            result = PRIME * result + matrix[i][j].hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (!isImmutable)
			throw IMMUTABLE_EXPECTED;
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Matrix other = (Matrix) obj;
		if (height != other.height)
			return false;
		if (width != other.width)
			return false;
		// can't use Arrays.equals() because it does not work for dimension > 1
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				if (!matrix[i][j].equals(other.matrix[i][j]))
					return false;
		return true;
	}

	@Override
	public Matrix mergeNumbers() {
		boolean createMatrix = false;
		Scalar[][] _matrix = null;
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++) {
				Scalar r = matrix[i][j];
				Scalar s = r.mergeNumbers();
				if (createMatrix) {
					_matrix[i][j] = s;
				} else if (s != r) {
					// create array
					_matrix = new Scalar[height][width];
					// copy all elements handled till now to the array
					// this elements are idempotent in the function "mergeNumbers"
					for (int k = 0; k <= i; k++) {
						int n = k == i ? j : width;
						for (int l = 0; l < n; l++) {
							_matrix[k][l] = matrix[k][l];
						}
					}
					// add new Element
					_matrix[i][j] = s;
					// change flag
					createMatrix = true;
				}
			}
		return createMatrix ? new Matrix(_matrix): this;
	}

	public Matrix invert() {
		return MatrixOperations.invert(this);
	}
	
	public Matrix transpose() {
		return Transpose.apply(this);
	}
	
	public Matrix multiply(Scalar s) {
		return Multiplication.apply(s, this);
	}
	
	public Matrix multiply(Matrix matrix) {
		return Multiplication.apply(this, matrix);
	}

	public Matrix add(Matrix matrix) {
		return Addition.apply(this, matrix);
	}

	public Matrix subtract(Matrix matrix) {
		return Subtraction.apply(this, matrix);
	}
	
	public static Matrix parse(String str) {
		return (Matrix) Operand.parse(str);
	}

	@Override
	public int getSize() {
		int size = 0;
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				size += matrix[i][j].getSize();
		return size;
	}

	@Override
	protected void getVariables(Collection<Variable> variables) {
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				matrix[i][j].getVariables(variables);
	}
}
