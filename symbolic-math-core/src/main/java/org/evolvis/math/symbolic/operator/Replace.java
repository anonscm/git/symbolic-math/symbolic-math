package org.evolvis.math.symbolic.operator;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Operator;
import org.evolvis.math.symbolic.Scalar;

public class Replace extends Operator {

	private static final String ID = "rplc";

	@Override
	protected boolean matchToken(String token) {
		return ID.equals(token);
	}

	@Override
	public int getInputCount() {
		return 3;
	}

	@Override
	protected Operand apply(String token, Operand... input) {
		return apply(input[0].asMatrix(), input[1].asMatrix(), input[2].asMatrix());
	}

	public static final Matrix apply(Matrix A, Matrix find, Matrix replace) {
		if (! find.isSameSize(replace) || find.getWidth() > 1)
			throw EXCEPTION_MATRIX_DIMENSION;
		int n = find.getHeight();
		int height = A.getHeight();
		int width = A.getWidth();
		Matrix B = new Matrix(height, width);
		for (int l = 0; l < n; l ++) {
			Scalar sf = find.getElement(l, 0);
			Scalar sr = replace.getElement(l, 0);
			for (int i = 0; i < height; i ++)
				for (int j = 0; j < width; j ++) {
					Scalar s = l == 0 ? A.getElement(i, j) : B.getElement(i, j);
					s = s.replaceScalar(sf, sr);
					B.setElement(i, j, s);
				}
		}
		return B.setImmutable();
	}
}