package org.evolvis.math.symbolic.operator.function;

import org.evolvis.math.symbolic.Function;
import org.evolvis.math.symbolic.FunctionType;
import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Scalar;

public class Negation extends FunctionType {

	private static final String ID = "~";
	
	private static FunctionType instance;

	public Negation() {
		instance = this;
	}
	
	@Override
	public String getName() {
		return ID;
	}

	@Override
	public int getInputCount() {
		return 1;
	}
	
	@Override
	protected Operand apply(Operand... input) {
		if (input[0] instanceof Scalar)
			return apply((Scalar) input[0]);
		return apply((Matrix) input[0]);
	}

	public static final Matrix apply(Matrix matrix) {
		Matrix matrix2 = new Matrix(matrix.getHeight(), matrix.getWidth());
		for (int i = 0; i < matrix.getHeight(); i++)
			for (int j = 0; j < matrix.getWidth(); j++)
				matrix2.setElement(i, j, 
					apply(matrix.getElement(i, j)));
		return matrix2.setImmutable();
	}

	public static final Scalar apply(Scalar node) {
		return new Function(instance, node);
	}

	@Override
	public Scalar getDerivate(String name, Scalar[] inputs) {
		// (x ~) --> (x) ~
		Scalar xd = inputs[0].getDerivate(name);
		return apply(xd);
	}
}
