package org.evolvis.math.symbolic.operator.function;

import org.evolvis.math.symbolic.Function;
import org.evolvis.math.symbolic.FunctionType;
import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Scalar;

public class Multiplication extends FunctionType {

	public static final String ID = "*";
	
	private static FunctionType instance;

	public Multiplication() {
		instance = this;
	}
	
	@Override
	public String getName() {
		return ID;
	}

	@Override
	public int getInputCount() {
		return 2;
	}
	
	@Override
	protected Operand apply(Operand... input) {
		if (input[0] instanceof Scalar) {
			if (input[1] instanceof Scalar) // standard multiplication
				return apply((Scalar) input[0], (Scalar) input[1]);
			else                       // scalar multiplication
				return apply((Scalar) input[0], (Matrix) input[1]);
		} else {
			if (input[1] instanceof Scalar) // scalar multiplication
				return apply((Scalar) input[1], (Matrix) input[0]);
			else                       // matrix multiplication
				return apply((Matrix) input[0], (Matrix) input[1]);
		}
	}

	public static final Scalar apply(Scalar s1, Scalar s2) {
		return new Function(instance, s1, s2);
	}
	
	/**
	 * Creates a new instance of {@link Matrix}.
	 * Apply function {@link #apply(Scalar, Scalar)} on every element of the
	 * matrices.
	 * 
	 * @param  node
	 * @param  matrix
	 * @return
	 */
	public static final Matrix apply(Scalar node, Matrix matrix) {
		Matrix matrix2 = new Matrix(matrix.getHeight(), matrix.getWidth());
		for (int i = 0; i < matrix.getHeight(); i++)
			for (int j = 0; j < matrix.getWidth(); j++)
				matrix2.setElement(i, j, 
					apply(node, matrix.getElement(i, j)));
		matrix2.setImmutable();
		return matrix2;
	}
	
	public static final Matrix apply(Matrix matrix1, Matrix matrix2) {
		if (matrix1.getWidth() != matrix2.getHeight())
			throw EXCEPTION_MATRIX_DIMENSION;
		Matrix matrix3 = new Matrix(matrix1.getHeight(), matrix2.getWidth());
		for (int i = 0; i < matrix1.getHeight(); i++)
			for (int j = 0; j < matrix2.getWidth(); j++) {
				Scalar node = null;
				for (int k = 0; k < matrix1.getWidth(); k++) {
					Scalar node2 = Multiplication.apply(
									matrix1.getElement(i, k), 
									matrix2.getElement(k, j));
					node = (node == null ? node2 : Addition.apply(node, node2));
				}
				matrix3.setElement(i, j, node); 
			}
		//matrix3.addConstaintList(matrix1.getConstaintList());
		//matrix3.addConstaintList(matrix2.getConstaintList());
		return matrix3.setImmutable();
	}

	@Override
	public Scalar getDerivate(String name, Scalar[] inputs) {
		// (x y *) --> (x) y * x (y) * +
		Scalar x = inputs[0], y = inputs[1];
		Scalar xd = x.getDerivate(name);
		Scalar yd = y.getDerivate(name);
		return Addition.apply(Multiplication.apply(xd, y),
				Multiplication.apply(x, yd));
	}
}
