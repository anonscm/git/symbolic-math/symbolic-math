package org.evolvis.math.symbolic;

import java.util.Arrays;
import java.util.Collection;

import org.evolvis.math.symbolic.FunctionType.ParameterType;
import org.evolvis.math.symbolic.operator.function.Addition;
import org.evolvis.math.symbolic.operator.function.Division;
import org.evolvis.math.symbolic.operator.function.Inversion;
import org.evolvis.math.symbolic.operator.function.Multiplication;
import org.evolvis.math.symbolic.operator.function.Negation;
import org.evolvis.math.symbolic.operator.function.Subtraction;
import org.evolvis.math.symbolic.operator.function.condition.EqualTo;
import org.evolvis.math.symbolic.operator.function.condition.IfElse;


public class Function extends Scalar {
	
	protected FunctionType type;
	protected Scalar[] inputs;
	private int size = 0; // store size for better performance
	private Scalar mergedScalar = null;
	
	/**
	 * Pro: an invalid formula will be noticed more easily
	 * Contra: Transformations need to be changed, so it is possible to use boolean functions
	 */
	private static final boolean CHECK_PRAMETER_TYPES = false;
	
	public Function(FunctionType type) {
		this(type, (Scalar[]) null);
	}
	
	public Function(FunctionType type, Scalar... inputs) {
		if (type == null)
			throw new NullPointerException();
		this.type = type;
		if (inputs == null)
			this.inputs = new Scalar[type.getInputCount()];
		else {
			if (type.getInputCount() != inputs.length)
				throw new RuntimeException("wrong input size");
			this.inputs = inputs;
			setImmutable();
		}
	}
	
	public static Function create(String str) {
		Scalar op = Scalar.create(str);
		if (!(op instanceof Function))
			throw SERIALIZATION_WRONG_TYPE;
		return (Function) op;
	}
	
	@Override
	public Function setImmutable() {
		for (int i = 0; i < inputs.length; i++)
			if (inputs[i] == null) // access directly because the function is not initialized yet
				throw new RuntimeException("input containes a null element");
			else {
				// check types for function paramters
				if (CHECK_PRAMETER_TYPES) { 
					if (inputs[i] instanceof Function) {
						Function func = (Function) inputs[i];
						if (func.getType().getOutputType() != type.getInputType(i))
							throw new RuntimeException("input "+i+" requires a differnet parameter type");
					} else if (inputs[i] instanceof Number || inputs[i] instanceof Variable) {
						if (type.getInputType(i) != ParameterType.RATIONAL)
							throw new RuntimeException("input "+i+" requires a differnet parameter type");
					}
				}
			}
		return (Function) super.setImmutable();
	}
	
	public void setInput(int i, Scalar input) {
		if (isImmutable)
			throw IMMUTABLE_IS_READONLY;
		inputs[i] = input;
	}
	
	public Scalar getInput(int i) {
		return inputs[i];
	}
	
	public Scalar[] getInputs() {
		return inputs;
	}

	public int getInputCount() {
		return inputs.length;
	}

	public FunctionType getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + Arrays.hashCode(inputs);
		result = PRIME * result + type.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Function other = (Function) obj;
		if (!Arrays.equals(inputs, other.inputs))
			return false;
		return type.equals(other.type);
	}

	@Override
	public int getSize() {
		if (size == 0) { // first call of this method
			size = 1;
			for (int i = 0; i < getInputCount(); i++)
				size += getInput(i).getSize();
		}
		return size;
	}

	@Override
	public Scalar selectNode(int idx) {
		if (idx == 0)
			return this;
		int offset = 1;
		for (int i = 0; i < getInputCount(); i++) {
			Scalar sNode = getInput(i).selectNode(idx-offset);
			if (sNode != null) // done!
				return sNode;
			else
				offset += getInput(i).getSize();
		}
		return null;
	}

	@Override
	public Scalar replaceNode(int idx, Scalar scalar) {
		if (idx < 0)
			return this;
		if (idx == 0)
			return equals(scalar) ? this : scalar;
		int offset = 1;
		for (int i = 0; i < getInputCount(); i++) {
			Scalar input = getInput(i);
			Scalar sNode = input.replaceNode(idx-offset, scalar);
			if (sNode != input) { // replaced input => clone current function (but not the inputs)
				Function func = new Function(getType());
				for (int j = 0; j < getInputCount(); j++)
					func.setInput(j, i == j ? sNode : getInput(j));
				func.setImmutable();
				return func;
			} else
				offset += getInput(i).getSize();
		}
		return this;
	}

	@Override
	public Scalar getDerivate(String name) {
		return type.getDerivate(name, inputs);
	}

	@Override
	public boolean isAtomic() {
		return false;
	}

	private Scalar mergeNumbers2() {
		boolean returnNew = false;
		Scalar[] inputs = this.inputs;
		for (int i = 0; i < getInputCount(); i++) {
			Scalar in = getInput(i);
			Scalar _in = in.mergeNumbers();
			if (_in.equals(Number.NAN))
				return Number.NAN;
			if (returnNew) // new function => set input
				inputs[i] = _in;
			else if (in != _in) { // input changed => return new function
				inputs = new Scalar[getInputCount()];
				for (int j = 0; j < i; j++)
					inputs[j] = getInput(j);
				inputs[i] = _in;
				returnNew = true;
			}
		}
		if (type instanceof Addition) {
			if (inputs[0] instanceof Number) {
				if (Number.ZERO.equals(inputs[0]))
					return inputs[1];
				if (inputs[1] instanceof Number)
					return ((Number) inputs[0]).add((Number) inputs[1]);
			}
			if (Number.ZERO.equals(inputs[1]))
				return inputs[0];
		} else if (type instanceof Subtraction) {
			if (inputs[0] instanceof Number) {
				if (inputs[1] instanceof Number)
					return ((Number) inputs[0]).subtract((Number) inputs[1]);
				if (Number.ZERO.equals(inputs[0]))
					return Negation.apply(inputs[1]);
			}
			if (Number.ZERO.equals(inputs[1]))
				return inputs[0];
		} else if (type instanceof Multiplication) {
			if (inputs[0] instanceof Number) {
				if (Number.ONE.equals(inputs[0]))
					return inputs[1];
				if (Number.ZERO.equals(inputs[0]))
					return Number.ZERO;
				if (inputs[1] instanceof Number)
					return ((Number) inputs[0]).multiply((Number) inputs[1]);
				if (Number.ONE_NEG.equals(inputs[0]))
					return Negation.apply(inputs[1]);
			}
			if (Number.ONE.equals(inputs[1]))
				return inputs[0];
			if (Number.ZERO.equals(inputs[1]))
				return Number.ZERO;
			if (Number.ONE_NEG.equals(inputs[1]))
				return Negation.apply(inputs[0]);
		} else if (type instanceof Division) {
			if (inputs[1] instanceof Number) {
				if (Number.ZERO.equals(inputs[1]))
					return Number.NAN;
				if (Number.ONE.equals(inputs[1]))
					return inputs[0];
				if (inputs[0] instanceof Number)
					return ((Number) inputs[0]).divide((Number) inputs[1]);
				if (Number.ONE_NEG.equals(inputs[1]))
					return Negation.apply(inputs[0]); // swap possible
			}
			if (inputs[0] instanceof Number) {
				if (Number.ONE.equals(inputs[0]))
					return Inversion.apply(inputs[1]);
				if (Number.ZERO.equals(inputs[0]))
					return Number.ZERO;
				if (Number.ONE_NEG.equals(inputs[0]))
					return Negation.apply(Inversion.apply(inputs[1])); // swap possible
			}
		} else if (type instanceof Negation) {
			if (inputs[0] instanceof Number)
				return ((Number) inputs[0]).negate();
		} else if (type instanceof Inversion) {
			if (inputs[0] instanceof Number)
				return ((Number) inputs[0]).invert();
		} else if (type instanceof EqualTo) {
			if (inputs[0] instanceof Number && inputs[1] instanceof Number)
				return inputs[0].equals(inputs[1]) ? Number.ONE : Number.ZERO;
			if (inputs[0].equals(inputs[1]) )
				return Number.ONE; // true;
		} else if (type instanceof IfElse) {
			if (inputs[0].equals(Number.ZERO))
				return inputs[2];
			if (inputs[0].equals(Number.ONE))
				return inputs[1];
		}
		return returnNew ? new Function(type, inputs) : this;
	}

	@Override
	public Scalar mergeNumbers() {
		if (mergedScalar == null)
			mergedScalar = mergeNumbers2();
		return mergedScalar;
	}

	@Override
	protected void getVariables(Collection<Variable> variables) {
		for (int i = 0; i < inputs.length; i++)
			inputs[i].getVariables(variables);
	}

	@Override
	public Scalar replaceScalar(Scalar find, Scalar replace) {
		if (equals(find))
			return replace;
		boolean returnNew = false;
		Scalar[] inputs = this.inputs;
		for (int i = 0; i < getInputCount(); i++) {
			Scalar in = getInput(i);
			Scalar _in = in.replaceScalar(find, replace);
			if (returnNew) // new function => set input
				inputs[i] = _in;
			else if (in != _in) { // input changed => return new function
				inputs = new Scalar[getInputCount()];
				for (int j = 0; j < i; j++)
					inputs[j] = getInput(j);
				inputs[i] = _in;
				returnNew = true;
			}
		}
		return returnNew ? new Function(type, inputs) : this;
	}
}
