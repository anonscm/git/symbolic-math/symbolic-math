package org.evolvis.math.symbolic.operator;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Number;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Operator;
import org.evolvis.math.symbolic.Scalar;

public class Dimension extends Operator {

	private static final String ID_HEIGHT = "height";
	private static final String ID_WIDTH = "width";

	@Override
	protected boolean matchToken(String token) {
		return ID_HEIGHT.equals(token) || ID_WIDTH.equals(token);
	}

	@Override
	public int getInputCount() {
		return 1;
	}
	
	@Override
	protected Operand apply(String token, Operand... input) {
		if (ID_HEIGHT.equals(token))
			return applyGetHeight(input[0]);
		return applyGetWidth(input[0]);
	}

	public static final Number applyGetHeight(Operand op) {
		if (op instanceof Scalar)
			return Number.ONE;
		return Number.create(((Matrix)op).getHeight());
	}

	public static final Number applyGetWidth(Operand op) {
		if (op instanceof Scalar)
			return Number.ONE;
		return Number.create(((Matrix)op).getWidth());
	}
}