package org.evolvis.math.symbolic.traverse;

import org.evolvis.math.symbolic.Function;
import org.evolvis.math.symbolic.FunctionType;
import org.evolvis.math.symbolic.Number;
import org.evolvis.math.symbolic.Scalar;
import org.evolvis.math.symbolic.Variable;
import org.evolvis.math.symbolic.operator.function.Addition;
import org.evolvis.math.symbolic.operator.function.Cosinus;
import org.evolvis.math.symbolic.operator.function.Division;
import org.evolvis.math.symbolic.operator.function.Inversion;
import org.evolvis.math.symbolic.operator.function.Multiplication;
import org.evolvis.math.symbolic.operator.function.Negation;
import org.evolvis.math.symbolic.operator.function.Sinus;
import org.evolvis.math.symbolic.operator.function.Subtraction;
import org.evolvis.math.symbolic.operator.function.condition.EqualTo;
import org.evolvis.math.symbolic.operator.function.condition.IfElse;

public class CalculationErrorCostCalculator extends OperandCostCalculator {

	private static final double COST_MULTIPLICATION = 0.01;
	private static final double GAIN_MULTIPLICATION = 1.1;
	
	private static final double COST_INVERSION = 0.011;
	
	private static final double COST_ADDITION = 0.1;
	private static final double GAIN_ADDITION = 1.7;
	private static final double COST_SUBTRACTION = 0.100001;

	private static final double COST_NEGATION = 0.0001;
	
	private static final double COST_SINUS_COSINUS = 0.07;
	private static final double GAIN_SINUS_COSINUS = 1.1;

	protected double getCost(Scalar scalar) {
		if (scalar instanceof Number)
			return 0;
		if (scalar instanceof Variable)
			return 0;
		// scalar instanceof Function
		Function f = (Function) scalar;
		FunctionType type = f.getType();
		if (type instanceof Multiplication || type instanceof Division)
			return (getCost(f.getInput(0))+getCost(f.getInput(1)))
			* GAIN_MULTIPLICATION + COST_MULTIPLICATION;
		if (type instanceof Addition)
			return (getCost(f.getInput(0))+getCost(f.getInput(1)))
			* GAIN_ADDITION + COST_ADDITION;
		if (type instanceof Subtraction)
			return (getCost(f.getInput(0))+getCost(f.getInput(1)))
			* GAIN_ADDITION + COST_SUBTRACTION;
		if (type instanceof Inversion)
			return getCost(f.getInput(0))
			* GAIN_MULTIPLICATION + COST_INVERSION;
		if (type instanceof Negation)
			return getCost(f.getInput(0)) + COST_NEGATION;
		if (type instanceof Sinus || type instanceof Cosinus)
			return getCost(f.getInput(0))
			* GAIN_SINUS_COSINUS + COST_SINUS_COSINUS;
		if (type instanceof IfElse)
			return getCost(f.getInput(0)) + getCost(f.getInput(1)) + getCost(f.getInput(2));
		if (type instanceof EqualTo)
			return getCost(f.getInput(0)) + getCost(f.getInput(1));
		throw new RuntimeException("unhandled function type: "+type.getName());
	}
}
