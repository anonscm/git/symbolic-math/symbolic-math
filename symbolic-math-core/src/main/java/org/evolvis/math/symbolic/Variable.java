package org.evolvis.math.symbolic;

import java.util.Collection;

/**
 * A variable is an immutable child node in a function tree.
 * 
 * @author Hendrik Helwich
 */
public class Variable extends Scalar {

	private final String name;

	Variable(final String name) throws OperandInitializationException {
		this.name = name;
		setImmutable();
	}

	public static Variable create(String str) {
		Scalar s = Scalar.create(str);
		if (!(s instanceof Variable))
			throw SERIALIZATION_WRONG_TYPE;
		return (Variable) s;
	}

	public final String getName() {
		return name;
	}
	
	@Override
	public final int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + name.hashCode();
		return result;
	}

	@Override
	public final boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Variable other = (Variable) obj;
		return other.name.equals(name);
	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public Scalar selectNode(int idx) {
		return idx == 0 ? this : null;
	}

	@Override
	public Scalar replaceNode(int idx, Scalar scalar) {
		return idx == 0 && !equals(scalar) ? scalar : this;
	}

	@Override
	public boolean isAtomic() {
		return true;
	}

	@Override
	public Scalar mergeNumbers() {
		return this;
	}

	@Override
	protected void getVariables(Collection<Variable> variables) {
		variables.add(this);
	}

	@Override
	public Scalar replaceScalar(Scalar find, Scalar replace) {
		return equals(find) ? replace : this;
	}
}
