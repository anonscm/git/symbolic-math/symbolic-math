package org.evolvis.math.symbolic.operator.function.condition;

import org.evolvis.math.symbolic.Function;
import org.evolvis.math.symbolic.FunctionType;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Scalar;

/**
 * singleton
 * 
 * @author Hendrik Helwich
 */
public class EqualTo extends FunctionType {
	
	private static final String ID = "==";
	
	private static FunctionType instance;

	public EqualTo() {
		instance = this;
	}
	
	@Override
	public String getName() {
		return ID;
	}
	
	@Override
	public int getInputCount() {
		return 2;
	}

	@Override
	public Operand apply(Operand... input) {
		if (input[0] instanceof Scalar && input[1] instanceof Scalar)
			return apply((Scalar) input[0], (Scalar) input[1]);
		else
			throw EXCEPTION_WRONG_INPUT_TYPE;
	}
	
	public static final Scalar apply(Scalar s1, Scalar s2) {
		return new Function(instance, s1, s2);
	}

	@Override
	public Scalar getDerivate(String name, Scalar[] inputs) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ParameterType getOutputType() {
		return ParameterType.BOOLEAN;
	}
}
