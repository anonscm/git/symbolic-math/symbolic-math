package org.evolvis.math.symbolic.traverse;

import java.util.HashMap;
import java.util.Map;

import org.evolvis.math.symbolic.Function;
import org.evolvis.math.symbolic.FunctionType;
import org.evolvis.math.symbolic.Number;
import org.evolvis.math.symbolic.Scalar;
import org.evolvis.math.symbolic.Variable;

/**
 * @author Hendrik Helwich
 */
class ScalarTransformation {
	
	/**
	 * A {@link String} which separates the matching rule and the
	 * transformation rule when they are passed to the constructor as one
	 * {@link String}.
	 */
	public static final String SEPARATOR = "-->";
	
	private final Scalar match, transform;
	
	private final boolean expanding;

	/**
	 * @param rule
	 */
	public ScalarTransformation(String rule) {
		int idx = rule.indexOf(SEPARATOR);
		if (idx == -1)
			throw new RuntimeException("invalid transformation rule");
		String left = rule.substring(0, idx);
		String right = rule.substring(idx + SEPARATOR.length());
		match = Scalar.parse(left);
		transform = Scalar.parse(right);
		validate(match, transform);
		expanding = match.getSize() < transform.getSize();
	}
	
	private static final void validate(Scalar match, Scalar transform) {
		//TODO: check: variable names, length 1, variables in right must be contained in left
	}
	
	/**
	 * Apply the transformation rule which is represented by an instance of
	 * this class on the given {@link Scalar}.
	 * If the given {@link Scalar} does match the constraints of this rule it
	 * will be transformed to the composition which is specified by this
	 * wtransformation rule and the transformed {@link Scalar} will be
	 * returned.
	 * If the given {@link Scalar} does not match this rule, it will be
	 * returned instead.
	 * 
	 * @param  func
	 *         A {@link Scalar} on which the transformation rule which is
	 *         represented by an instance of this class should be applied.
	 * @return The transformed {@link Scalar} of the given {@link Scalar}
	 *         or the given {@link Scalar} itself if it does not match this
	 *         rule. 
	 */
	public Scalar apply(final Scalar func) {
		Map<Character, Scalar> bind = new HashMap<Character, Scalar>();
		if (! match(bind, match, func)) // scalar does not match the rule
			return func;
		return apply(bind, transform);
	}

	private static Scalar apply(final Map<Character, Scalar> bind, final Scalar r) {
		if (r instanceof Number) // number
			return r;
		else if (r instanceof Variable) { // variable
			Variable v = (Variable) r;
			char ch = v.getName().charAt(0);
			return bind.get(ch);
		} else { // function
			Function func = (Function) r;
			FunctionType type = func.getType();
			Function fnew = new Function(type);
			for (int i = 0; i < type.getInputCount(); i ++)
				fnew.setInput(i, apply(bind, func.getInput(i)));
			return fnew.setImmutable();
		}
	}

	/**
	 * It will be checked if the first scalar parameter matches the second
	 * scalar parameter. In this case the method will return <code>true</code>
	 * and otherwise <code>false</code>.
	 * A {@link Number} node in the matching scalar will only match the same
	 * {@link Number} in the tested scalar node.
	 * A {@link Variable} node in the matching scalar must have a name of
	 * length 1. If this character is between 'i' and 'n' it will match any
	 * {@link Number} node in the tested scalar node. If this character is
	 * between 'p' and 'z' it will match any node in the tested scalar node.
	 * A {@link Function} node will only match a {@link Function} node with the
	 * same {@link FunctionType} and which children all match the above rules.
	 * This method will be called recurive for the nodes in the
	 * <code>test</code> scalar and the <code>match</code> scalar. 
	 * 
	 * @param  bind
	 * @param  match
	 * @param  test
	 * @return
	 */
	private static boolean match(Map<Character, Scalar> bind, final Scalar match, final Scalar test) {
		if (match instanceof Number) { // number
			if (! match.equals(test))
				return false;
		} else if (match instanceof Variable) { // variable
			Variable v = (Variable) match;
			char ch = v.getName().charAt(0);
			if (isNumberId(ch)) { // number
				if (! (test instanceof Number))
					return false;
			}
			if (bind == null) {
				bind = new HashMap<Character, Scalar>();
				bind.put(ch, test);
			} else {
				if (bind.containsKey(ch)) {
					Scalar l = bind.get(ch);
					if (! l.equals(test))
						return false;
				} else
					bind.put(ch, test);
			}
		} else { // function
			if (!(test instanceof Function))
				return false;
			Function fr = (Function) match;
			Function fs = (Function) test;
			FunctionType type = fr.getType();
			if (fs.getType() != type)
				return false;
			// function does match => check childs
			for (int i = 0; i < type.getInputCount(); i ++) {
				if (! match(bind, fr.getInput(i), fs.getInput(i))) // does not match
					return false;
			}
		}
		return true;
	}
	
	private static final boolean isNumberId(char ch) {
		return ch >= 'i' && ch <= 'n';
	}
	
	@SuppressWarnings("unused")
	private static final boolean isAnyId(char ch) {
		return ch >= 'p' && ch <= 'z';
	}
	
	public boolean isExpanding() {
		return expanding;
	} 
	
	@Override
	public String toString() {
		return match + "  " + SEPARATOR + "  " + transform;
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + match.hashCode();
		result = PRIME * result + transform.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ScalarTransformation other = (ScalarTransformation) obj;
		return match.equals(other.match) && transform.equals(other.transform);
	}
}
