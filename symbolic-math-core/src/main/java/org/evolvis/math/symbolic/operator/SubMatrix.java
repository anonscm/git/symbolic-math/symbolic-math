package org.evolvis.math.symbolic.operator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Operator;

public class SubMatrix extends Operator {

	private static final Pattern PATTERN = Pattern.compile("\\[(([1-9][0-9]*)(\\-([1-9][0-9]*))?(,([1-9][0-9]*)(\\-([1-9][0-9]*))?)?)\\]");
	
	@Override
	protected boolean matchToken(String token) {
		return PATTERN.matcher(token).matches();
	}

	@Override
	public int getInputCount() {
		return 1;
	}

	@Override
	protected Operand apply(String token, Operand... input) {
		Matrix A = input[0].asMatrix();
		Matcher m = PATTERN.matcher(token);
		if (!m.matches()) // assured that this is always false but the call is needed for m.group(i)
			throw new RuntimeException("illegal matrix index: "+token);
		boolean is1D = m.group(5) == null;
		if (is1D)
			if (A.getHeight() == 1) { // swap indexes (syntactic sugar)
				int cs = Integer.parseInt(m.group(2)); // mandatory
				String str = m.group(4);
				int ce = str != null ? Integer.parseInt(str) : cs;
				return apply(A, 1, 1, cs, ce);
			} else if (A.getWidth() != 1)
				throw new RuntimeException("second index component is missing: "+token);
		// index is 2 dimensional or ( index is 1 dimensional and matrix is column vector )
		int rs = Integer.parseInt(m.group(2)); // mandatory
		String str = m.group(4);
		int re = str != null ? Integer.parseInt(str) : rs;
		if (is1D) 
			return apply(A, rs, re, 1, 1);
		// index is 2 dimensional
		int cs = Integer.parseInt(m.group(6)); // mandatory
		str = m.group(8);
		int ce = str != null ? Integer.parseInt(str) : cs;
		return apply(A, rs, re, cs, ce);
	}

	public static final Matrix apply(Matrix A, int rowStart, int rowEnd, int columnStart, int columnEnd) {
		return A.getSubMatrix(rowStart-1, columnStart-1, rowEnd-rowStart+1, columnEnd-columnStart+1);
	}
}