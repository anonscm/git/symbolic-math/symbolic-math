package org.evolvis.math.symbolic;

/**
 * This {@link RuntimeException} can occur while initializing a subclass of the class
 * {@link Operand}.
 * 
 * @author Hendrik Helwich
 */
@SuppressWarnings("serial")
public class OperandInitializationException extends RuntimeException {

	public OperandInitializationException(String text) {
		super(text);
	}
}
