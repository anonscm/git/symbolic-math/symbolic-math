package org.evolvis.math.symbolic.environment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.OperandInitializationException;
import org.evolvis.math.symbolic.OperandSerializer;
import org.evolvis.math.symbolic.Variable;

/**
 * Holds {@link Operand}s with a specified name.
 * If a {@link Matrix} has an element of type {@link Variable} and the name of
 * this variable does start with the name of the matrix, the position and the
 * name of this matrix are stored for this variable.
 * This can be used to create methods which has matrices as an argument when
 * generating code form an {@link OperandEnvironmentSerializer}. 
 * 
 * @author Hendrik Helwich
 */
class OperandEnvironmentSerializer {
	
	/**
	 * The same as {@link OperandSerializer#PATTERN_VARIABLE_STRING}.
	 */
	private static final String PATTERN_VARIABLE_STRING = "[A-Za-z][A-Za-z0-9_]*";
	private static final Pattern PATTERN_ASSIGNMENT = Pattern.compile("\\s*("+PATTERN_VARIABLE_STRING+")\\s*=\\s*(.+)\\s*");

	static OperandEnvironment parse(OperandEnvironment env, Reader reader) throws OperandInitializationException {
		BufferedReader br = new BufferedReader(reader);
		try {
			for (String line = br.readLine(); line != null; line = br.readLine())
				env = parse(env, line);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return env;
	}
	
	static OperandEnvironment parse(OperandEnvironment env, String line) throws OperandInitializationException {
		if (env == null)
			env = new OperandEnvironment();
		Matcher m = PATTERN_ASSIGNMENT.matcher(line);
		if (m.matches()) {
			String name = m.group(1);
			String opStr = m.group(2);
			Operand op;
			try {
				op = Operand.parse(env, opStr);
			} catch (RuntimeException e) {
				throw new RuntimeException("error in line:\n"+line, e);
			}
			op = op.mergeNumbers();
			env.put(name, op);
		} else
			throw new RuntimeException("expected an assignment: "+line);
		return env;
	}
}