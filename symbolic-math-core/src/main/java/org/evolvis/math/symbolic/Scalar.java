package org.evolvis.math.symbolic;

import java.util.Random;

import org.evolvis.math.symbolic.operator.function.Addition;
import org.evolvis.math.symbolic.operator.function.Division;
import org.evolvis.math.symbolic.operator.function.Inversion;
import org.evolvis.math.symbolic.operator.function.Multiplication;
import org.evolvis.math.symbolic.operator.function.Negation;
import org.evolvis.math.symbolic.operator.function.Subtraction;

/**
 * {@link Function}, {@link Number}, {@link Variable}
 * 
 * @author Hendrik Helwich
 */
public abstract class Scalar extends Operand implements Comparable<Scalar> {
	
	/**
	 * If this returns <code>false</code>, the specified {@link Scalar}
	 * is an instance of {@link Function}.
	 * If <code>true</code> is returned it is an instance of
	 * {@link Number} or an instance of {@link Variable}.
	 * The atomic nodes are also the immutable nodes.
	 * 
	 * @param  node
	 * @return
	 */
	public abstract boolean isAtomic();


	public abstract Scalar selectNode(int idx);

	public static Scalar create(String str) {
		Operand op = Operand.create(str).tryScalar();
		if (!(op instanceof Scalar))
			throw SERIALIZATION_WRONG_TYPE;
		return (Scalar) op;
	}
	
	/**
	 * Replaces the scalar with the specified index with the specified scalar.
	 * If both nodes are equal, no new instances are generated. In the other
	 * case, all {@link Function}s above the replaced node must be cloned.
	 * 
	 * @param  idx
	 * @param  scalar
	 * @return
	 */
	public abstract Scalar replaceNode(int idx, Scalar scalar);
	
	/**
	 * Used by {@link #selectRandomNode()}.
	 */
	private static final Random random = new Random();
	
	public Scalar selectRandomNode() {
		int idx = random.nextInt(getSize());
		return selectNode(idx);
	}
	
	public Scalar getDerivate(String name) {
		if (this instanceof Number)
			return Number.ZERO;
		if (this instanceof Variable)
			return ((Variable)this).getName().equals(name) ?
					Number.ONE : Number.ZERO;
		Function func = (Function) this;
		return func.getDerivate(name);
	}

	@Override
	public abstract Scalar mergeNumbers();

	public Scalar multiply(Scalar scalar) {
		return Multiplication.apply(this, scalar);
	}

	public Scalar subtract(Scalar scalar) {
		return Subtraction.apply(this, scalar);
	}

	public Scalar negate() {
		return Negation.apply(this);
	}

	public Scalar invert() {
		return Inversion.apply(this);
	}

	public Scalar add(Scalar scalar) {
		return Addition.apply(this, scalar);
	}

	public Scalar divide(Scalar scalar) {
		return Division.apply(this, scalar);
	}
	
	public static Scalar parse(String str) {
		return (Scalar) Operand.parse(str);
	}

	/*
	 * number < variable < function 
	 */
	public int compareTo(Scalar s) {
		if (s == null)
			throw new NullPointerException();
		if (this instanceof Number) {
			if (s instanceof Number) {
				Number n1 = (Number) this;
				Number n2 = (Number) s;
				if (n1.equals(n2))
					return 0; // this == s
				else
					return n1.subtract(n2).getNominator().intValue();
			} else  // s instanceof Variable || s instanceof Function
				return -1; // this < s
		} else if (this instanceof Variable) {
			if (s instanceof Number)
				return 1; // this > s
			else if (s instanceof Variable)
				return ((Variable)this).getName().compareTo(((Variable)s).getName());
			else // s instanceof Function
				return -1; // this < s
		} else { // this instanceof Function
			if (s instanceof Function) {
				Function f1 = (Function) this;
				Function f2 = (Function) s;
				FunctionType t1 = f1.getType(), t2 = f2.getType();
				if (t1 == t2) {
					for (int i = 0; i < t1.getInputCount(); i++) {
						int c = f1.getInput(i).compareTo(f2.getInput(i));
						if (c != 0)
							return c;
					}
					return 0; // this == s
				} else
					return f1.getType().getName().compareTo(f2.getType().getName());
			} else  // s instanceof Number || s instanceof Variable
				return 1; // this > s
		}
	}
	
	//TODO move to Operand (make it available for matrices to)
	public abstract Scalar replaceScalar(Scalar find, Scalar replace);
}
