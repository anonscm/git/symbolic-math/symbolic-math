package org.evolvis.math.symbolic.operator.function;

import org.evolvis.math.symbolic.Function;
import org.evolvis.math.symbolic.FunctionType;
import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Scalar;

public class Division extends FunctionType {

	public static final String ID = "/";
	
	private static FunctionType instance;

	public Division() {
		instance = this;
	}
	
	@Override
	public String getName() {
		return ID;
	}

	@Override
	public int getInputCount() {
		return 2;
	}
	
	@Override
	protected Operand apply(Operand... input) {
		Operand left = input[0], right = input[1];
		if (right instanceof Scalar) {
			if (left instanceof Scalar)
				return apply((Scalar) left, (Scalar) right);
			else
				return apply((Matrix) left, (Scalar) right);
		} else
			throw EXCEPTION_WRONG_INPUT_TYPE;
	}

	public static final Scalar apply(Scalar s1, Scalar s2) {
		return new Function(instance, s1, s2);
	}
	
	public static final Matrix apply(Matrix A, Scalar s) {
		int height = A.getHeight();
		int width = A.getWidth();
		Matrix B = new Matrix(height, width);
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				B.setElement(i, j, 
					apply(A.getElement(i, j), s));
		return B.setImmutable();
	}
	
	@Override
	public Scalar getDerivate(String name, Scalar[] inputs) {
		// (x y /) --> (x) y * x (y) * - y y * /
		Scalar x = inputs[0], y = inputs[1];
		Scalar xd = x.getDerivate(name);
		Scalar yd = y.getDerivate(name);
		return Division.apply(
			Subtraction.apply(
				Multiplication.apply(xd, y),
				Multiplication.apply(x, yd)
			),
			Multiplication.apply(y, y)
		);
	}
}
