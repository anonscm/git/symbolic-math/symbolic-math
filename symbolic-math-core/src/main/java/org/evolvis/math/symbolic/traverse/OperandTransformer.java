package org.evolvis.math.symbolic.traverse;

import java.util.LinkedList;
import java.util.List;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Scalar;

public class OperandTransformer {
	
	private MatrixTransformer mtr;
	private ScalarTransformer str;
	private Operand best;

	private List<TransformerUpdateListener<Operand>> updateListener;
	
	public void addUpdateListener(final TransformerUpdateListener<Operand> listener) {
		if (mtr != null) {
			mtr.addUpdateListener(new TransformerUpdateListener<Matrix>() {
				public void newTransformation(Matrix transformation, double cost) {
					listener.newTransformation(transformation, cost);
				}
			});
		} else if (str != null) {
			str.addUpdateListener(new TransformerUpdateListener<Scalar>() {
				public void newTransformation(Scalar transformation, double cost) {
					listener.newTransformation(transformation, cost);
				}
			});
		} else if (updateListener == null)
				updateListener = new LinkedList<TransformerUpdateListener<Operand>>();
			updateListener.add(listener);
	}
	
	public synchronized void start(Operand op) {
		start(op, true);
	}
	
	public synchronized void start(Operand op, boolean checkMinimal) {
		if (mtr != null || str != null)
			throw new RuntimeException("transformation is still running");
		best = op;
		if (op instanceof Matrix) {
			mtr = new MatrixTransformer();
			for (final TransformerUpdateListener<Operand> l : updateListener)
				mtr.addUpdateListener(new TransformerUpdateListener<Matrix>() {
					public void newTransformation(Matrix transformation, double cost) {
						best = transformation;
						l.newTransformation(transformation, cost);
					}
				});
			updateListener = null;
			mtr.start((Matrix) op, checkMinimal);
		} else {
			str = new ScalarTransformer((Scalar) op, checkMinimal);
			for (final TransformerUpdateListener<Operand> l : updateListener)
				str.addUpdateListener(new TransformerUpdateListener<Scalar>() {
					public void newTransformation(Scalar transformation, double cost) {
						best = transformation;
						l.newTransformation(transformation, cost);
					}
				});
			updateListener = null;
		}
	}
	
	public void stop() {
		if (mtr != null) {
			mtr.stop();
		} else if (str != null) {
			str.stop();
		} else
			throw new RuntimeException("no transformation is running");
	}
	
	public void wait(Matrix matrix, long time) {
		start(matrix);
		long startTime = System.currentTimeMillis();
		while (isRunning() && time > System.currentTimeMillis() - startTime) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public boolean isRunning() {
		if (mtr != null) {
			boolean running = mtr.isRunning();
			if (! running)
				mtr = null;
			return running;
		} else if (str != null) {
			boolean running = str.isRunning();
			if (! running)
				str = null;
			return running;
		} else
			return false;
	}
	
	public Operand getBest() {
		if (mtr != null)
			return mtr.getBest();
		else if (str != null)
			return str.getBest();
		else
			return best;
	}
}
