package org.evolvis.math.symbolic.traverse;

import org.evolvis.math.symbolic.Scalar;

public class SizeCostCalculator extends OperandCostCalculator {

	protected double getCost(Scalar scalar) {
		return scalar.getSize();
	}
}
