package org.evolvis.math.symbolic.operator;

import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Operator;

public class QuaternionMultiplication extends Operator {

	public static final String ID = "q*";

	@Override
	protected boolean matchToken(String token) {
		return ID.equals(token);
	}

	@Override
	public int getInputCount() {
		return 2;
	}

	@Override
	protected Operand apply(String token, Operand... input) {
		return apply(input[0].asMatrix(), input[1].asMatrix());
	}

	public static final Matrix apply(Matrix q1, Matrix q2) {
		if (! isQuaternion(q1) || ! isQuaternion(q2))
			throw EXCEPTION_MATRIX_DIMENSION;
		Matrix q = new Matrix(4, 1);
		q.setElement(0, 0, q1.getElement(0, 0).multiply(q2.getElement(0, 0))
			.subtract(q1.getElement(1, 0).multiply(q2.getElement(1, 0)))
			.subtract(q1.getElement(2, 0).multiply(q2.getElement(2, 0)))
			.subtract(q1.getElement(3, 0).multiply(q2.getElement(3, 0))));
		q.setElement(1, 0, q1.getElement(0, 0).multiply(q2.getElement(1, 0))
			.add(q1.getElement(1, 0).multiply(q2.getElement(0, 0)))
			.add(q1.getElement(2, 0).multiply(q2.getElement(3, 0)))
			.subtract(q1.getElement(3, 0).multiply(q2.getElement(2, 0))));
		q.setElement(2, 0, q1.getElement(0, 0).multiply(q2.getElement(2, 0))
			.add(q1.getElement(2, 0).multiply(q2.getElement(0, 0)))
			.add(q1.getElement(3, 0).multiply(q2.getElement(1, 0)))
			.subtract(q1.getElement(1, 0).multiply(q2.getElement(3, 0))));
		q.setElement(3, 0, q1.getElement(0, 0).multiply(q2.getElement(3, 0))
			.add(q1.getElement(3, 0).multiply(q2.getElement(0, 0)))
			.add(q1.getElement(1, 0).multiply(q2.getElement(2, 0)))
			.subtract(q1.getElement(2, 0).multiply(q2.getElement(1, 0))));
		return q.setImmutable();
	}
	
	//TODO move to util class
	static final boolean isQuaternion(Matrix q) {
		return q.getHeight() == 4 && q.getWidth() == 1;
	}
}
