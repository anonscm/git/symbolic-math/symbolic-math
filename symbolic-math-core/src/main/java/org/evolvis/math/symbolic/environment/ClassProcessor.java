package org.evolvis.math.symbolic.environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.Writer;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Hendrik Helwich
 */
public class ClassProcessor {
	
	private enum Position {
		begin, environment, waitcode, code, end
	}

	private static final Pattern PATTERN_BEGIN_ENV = Pattern.compile("\\s*/\\*\\^\\s*");
	private static final Pattern PATTERN_LINE_ENV = Pattern.compile("\\s*\\*\\s*([^#]*)(#.*)?");
	private static final Pattern PATTERN_END_ENV = Pattern.compile(".*\\*/\\s*");
	private static final Pattern PATTERN_BEGIN_CODE = Pattern.compile("\\s*/\\*\\^\\s*BEGIN\\s*GENERATED\\s*CODE\\s*\\*/\\s*");
	private static final Pattern PATTERN_END_CODE = Pattern.compile("\\s*/\\*\\^\\s*END\\s*GENERATED\\s*CODE\\s*\\*/\\s*");

	private static final Logger logger = Logger.getLogger(ClassProcessor.class.getName());
	

	//TODO don't overwerite file => flag for backup
	//TODO switch classFile and tempFile if classFile is newer
	public static boolean creatFunctions(File template, File classFile, boolean overwrite) throws IOException {
		if (template == null || classFile == null)
			throw new NullPointerException();
		if (!template.exists())
			throw new RuntimeException("file \""+template.getAbsolutePath()+"\" does not exist");
		if (classFile.exists() && !overwrite)
			throw new RuntimeException("file \""+classFile.getAbsolutePath()+"\" does already exist");
		overwrite = overwrite && classFile.exists(); // now it means: is it needed to overwrite a file?
		
		// check time stamps => has the template been modified?
		long time = template.lastModified();
		if (overwrite)
			if (time == classFile.lastModified())
				return false;
		// no classfile does exist or the template is newer than the classfile
		logger.info("Processing file: "+template.getAbsolutePath());
		
		Position pos = Position.begin;
		BufferedReader br = new BufferedReader(new FileReader(template));
		File tmpFile;
		if (overwrite) {
			tmpFile = new File(template.getAbsolutePath()+"_tmp");
			for (int i = 0; tmpFile.exists(); i++)
				tmpFile = new File(template.getAbsoluteFile()+"_tmp"+i);
		} else
			tmpFile = classFile;
			
		Writer bw = new FileWriter(tmpFile);

		StringBuilder sb = new StringBuilder();
		try {
			for (String line = br.readLine(); line != null; line = br.readLine()) {
				switch (pos) {
				case begin:
					if (PATTERN_BEGIN_ENV.matcher(line).matches())
						pos = Position.environment;
					break;
				case environment:
					if (PATTERN_END_ENV.matcher(line).matches())
						pos = Position.waitcode;
					else {
						Matcher m = PATTERN_LINE_ENV.matcher(line);
						if (m.matches()) {
							String s = m.group(1);
							if (s.length() > 0)
								sb.append(s).append(BreakingStringBuilder.LINE_BREAK);
						} else
							throw new RuntimeException("illegal line: "+line);
					}
					break;
				case waitcode:
					if (PATTERN_BEGIN_CODE.matcher(line).matches()) {
						bw.write(line);
						bw.write(BreakingStringBuilder.LINE_BREAK);
						writeCode(bw, sb.toString());
						bw.write(BreakingStringBuilder.LINE_BREAK);
						pos = Position.code;
					}
					break;
				case code:
					if (PATTERN_END_CODE.matcher(line).matches())
						pos = Position.end;
					break;
				case end:
					// do nothing
					break;
				}
				if (pos != Position.code) {
					bw.write(line);
					bw.write(BreakingStringBuilder.LINE_BREAK);
				}
			}
		} catch (IOException e) {
			tmpFile.delete();
			throw e;
		}
		br.close();
		bw.close();
		if (pos == Position.end) { // process is complete => take generated file
			if (overwrite) {
				if (!classFile.delete())
					throw new IOException("unable to delete file \""+classFile.getAbsolutePath()+"\"");
				if (!tmpFile.renameTo(classFile))
					throw new IOException("unable to rename file \""
							+ tmpFile.getAbsolutePath()+"\" to file \""
							+ classFile.getAbsolutePath()+"\"");
			}
			if (!classFile.setLastModified(time))
				throw new IOException("unable to modify time of file \""+classFile.getAbsolutePath()+"\"");
			return true;
		} else { // process is not complete => discard generated file
			if (!tmpFile.delete())
				throw new IOException("unable to delete file \""+tmpFile.getAbsolutePath()+"\"");
			return false;
		}
	}

	private static void writeCode(Writer writer, String string) throws IOException {
		OperandEnvironment env = new OperandEnvironment(new StringReader(string));
		CodeGenerator.createJavaCode(env, writer, 1);
	}
}
