package org.evolvis.math.symbolic.environment;

import java.io.IOException;
import java.io.Writer;
import java.math.BigInteger;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.evolvis.math.symbolic.Function;
import org.evolvis.math.symbolic.FunctionType;
import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.Number;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Scalar;
import org.evolvis.math.symbolic.Variable;
import org.evolvis.math.symbolic.operator.function.Addition;
import org.evolvis.math.symbolic.operator.function.Cosinus;
import org.evolvis.math.symbolic.operator.function.Division;
import org.evolvis.math.symbolic.operator.function.Inversion;
import org.evolvis.math.symbolic.operator.function.Multiplication;
import org.evolvis.math.symbolic.operator.function.Negation;
import org.evolvis.math.symbolic.operator.function.Sinus;
import org.evolvis.math.symbolic.operator.function.Subtraction;
import org.evolvis.math.symbolic.operator.function.condition.EqualTo;
import org.evolvis.math.symbolic.operator.function.condition.GreaterTo;
import org.evolvis.math.symbolic.operator.function.condition.IfElse;
import org.evolvis.math.symbolic.operator.function.condition.LowerTo;

/**
 * 
 * @author Hendrik Helwich
 */
//TODO implement correct braces for if-else, consitions and subtraction
public class CodeGenerator {
	
	private static final String BUFFER_NAME = "buffer";
	private static final boolean REDUCE_BRACES = false;

	public static String createJavaMethod(String name, Operand op) {
		return createJavaMethod(null, name, op);
	}

	public static String createJavaMethod(OperandEnvironment oe, String name, Operand op) {
		return createJavaMethod(oe, name, op, 1);
	}

	//TODO it is maybe usefull to add assert routines which check that the
	//     matrix input parameters (also buffer) have the right dimension.
	public static String createJavaMethod(OperandEnvironment env, String name, Operand op, int indent) {
		BreakingStringBuilder bsb = new BreakingStringBuilder(new StringBuilder(), indent);
		createJavaMethodHeader(env, bsb, name, op);
		if (op instanceof Matrix) { // dimesnsion > 1x1
			Matrix A = (Matrix) op;
			int height = A.getHeight();
			int width = A.getWidth();
			bsb.append("if (").append(BUFFER_NAME).append(" == null)").newLine(1);
			bsb.append(BUFFER_NAME).append(" = new double");
			if (height > 1)
				bsb.append('[').append(height).append(']');
			if (width > 1)
				bsb.append('[').append(width).append(']');
			bsb.append(';').newLine(-1);
			for (int i = 0; i < height; i++)
				for (int j = 0; j < width; j++) {
					Scalar s = A.getElement(i, j);
					bsb.append(BUFFER_NAME);
					if (height > 1)
						bsb.append('[').append(i).append(']');
					if (width > 1)
						bsb.append('[').append(j).append(']');
					bsb.append(" = ");
					createJavaMethodCode(env, bsb, name, s);
					bsb.append(';').newLine();
				}
			bsb.append("return ").append(BUFFER_NAME).append(';');
		} else {
			Scalar s = (Scalar) op;
			bsb.append("return ");
			createJavaMethodCode(env, bsb, name, s);
			bsb.append(';');
		}
		bsb.newLine(-1).append('}').forceBreak();
		return bsb.toString();
	}
	
	private static String getDimensionStr(Matrix A) {
		return A.getHeight() == 1 || A.getWidth() == 1 ? "[]" : "[][]";
	}
	
	private static void createJavaMethodHeader(OperandEnvironment oe, BreakingStringBuilder bsb, String name, Operand op) {
		List<String> vars = getVariableNames(op);
		List<String> usedMatrices = new LinkedList<String>(); // store written matrix parameters to avoid duplicates 
		bsb.forceBreak().newLine().append("private static double");
		if (op instanceof Matrix) {
			Matrix A = (Matrix) op;
			String sd = getDimensionStr(A); // 1d matrix ?
			bsb.append(sd);
			bsb.append(' ').append(name).append('(');
			bsb.append("double").append(sd).append(' ').append(BUFFER_NAME);
		} else
			bsb.append(' ').append(name).append('(');
		for (int i = 0; i < vars.size(); i++) {
			String vname = vars.get(i);
			if (oe != null && oe.isReference(vname) && ! vname.startsWith(name)) { // write matrix parameter if not already written
				String mname = oe.getReference(vname).getMatrixName();
				if (!usedMatrices.contains(mname)) { // write matrix parameter
					if (i > 0 || op instanceof Matrix)
						bsb.append(", ");
					bsb.append("double").append(
							getDimensionStr((Matrix) oe.get(mname)))
						.append(' ').append(mname);
					usedMatrices.add(mname);
				}	
			} else { // write scalar parameter
				if (i > 0 || op instanceof Matrix)
					bsb.append(", ");
				bsb.append("double "+vars.get(i));
			}
		}
		bsb.append(") {").newLine(1);
	}

	public static String createJavaConstant(String name, Operand op, int indent) {
		BreakingStringBuilder bsb = new BreakingStringBuilder(new StringBuilder(), indent);
		bsb.forceBreak().newLine();
		if (op instanceof Scalar) {
			if (op instanceof Number) {
				bsb.append("private static final double "+name+" = "+((Number)op).toDouble()+";").appendBreak();
			} else
				throw new RuntimeException("operand "+op+" is not static");
		} else {
			bsb.append("private static final double")
				.append(getDimensionStr((Matrix) op))
				.append(' ').append(name).append(" = {").newLine(1);
			Matrix m = (Matrix) op;
			int height = m.getHeight(), width = m.getWidth();
			boolean isVector = height == 1 || width == 1;
			for (int i = 0; i < height; i++) {
				if (i > 0)
					if (isVector)
						bsb.append(", ");
					else
						bsb.append(',').newLine();
				if (!isVector)
					bsb.append("{");
				for (int j = 0; j < width; j++) {
					Scalar s = m.getElement(i, j);
					if (s instanceof Number) {
						if (j > 0)
							bsb.append(", ");
						bsb.append(Double.toString(((Number)s).toDouble()));
					} else
						throw new RuntimeException("Scalar "+s+" is not static");
				}
				if (!isVector)
					bsb.append("}");
			}
			bsb.newLine(-1).append("};").appendBreak();
		}
		return bsb.toString();
	}
	
	public static void createJavaCode(OperandEnvironment env, Writer writer, int indent) throws IOException {
		BreakingStringBuilder bsb = new BreakingStringBuilder(new StringBuilder(), indent);
		for (String name : env.nameSet()) {
			Operand op = env.get(name);
			bsb.forceBreak().indent().append("// ").appendNoWrap(name).appendNoWrap(" = ").appendNoWrap(op.toString());
			writer.write(bsb.toString());
			bsb.clear();
			if (isStatic(env, op))
				writer.write(createJavaConstant(name, op, indent));
			else
				writer.write(createJavaMethod(env, name, op, indent));
		}
		for (String name : env.referenceSet()) {
			VariableMatrixReference vmr = env.getReference(name);
			String mname = vmr.getMatrixName();
			String sname = name.substring(mname.length());
			MatrixIndex idx = selectMatrixIndex(vmr);
			Matrix A = (Matrix) env.get(mname);
			String sd = getDimensionStr(A); // 1d matrix ?
			
			// create getter
			bsb.forceBreak().newLine().append("private static double ");
			bsb.append("get_").append(mname).append('_')
				.append(sname).append('(');
			bsb.append("double").append(sd).append(' ').append(mname).append(") {");
			bsb.newLine(1).append("return ").append(mname);
			appendMatrixIndexString(bsb, A, idx);
			bsb.append(';').newLine(-1).append('}').newLine();
			
			// create setter
			bsb.forceBreak().newLine().append("private static void ");
			bsb.append("set_").append(mname).append('_')
				.append(sname).append('(');
			bsb.append("double").append(sd).append(' ').append(mname)
				.append(", ").append("double ").append(name).append(") {");
			bsb.newLine(1).append(mname);
			appendMatrixIndexString(bsb, A, idx);
			bsb.append(" = ").append(name).append(';')
				.newLine(-1).append('}').newLine();
			
			// write getter and setter
			writer.write(bsb.toString());
			bsb.clear();
		}
	}
	
	private static boolean isStatic(OperandEnvironment env, Operand op) {
		if (op instanceof Scalar)
			return op instanceof Number;
		Matrix m = (Matrix) op;
		for (int i = m.getHeight() - 1; i >= 0; i--)
			for (int j = m.getWidth() - 1; j >= 0; j--)
				if (!(m.getElement(i, j) instanceof Number))
					return false;
		return true;
	}
	
	private static void createJavaMethodCode(OperandEnvironment env, BreakingStringBuilder sb, String name, Scalar s) {
		createJavaMethodCode(env, sb, name, s, null, true);
	}
	
	private static MatrixIndex selectMatrixIndex(VariableMatrixReference vmr) {
		return vmr.getMatrixIndices().get(0);//TODO other strategies are possible
	}
	
	private static void appendMatrixIndexString(BreakingStringBuilder sb, Matrix A, MatrixIndex idx) {
		if (A.getHeight() > 1)
			sb.append('[').append(idx.getRow()).append(']');
		if (A.getWidth() > 1)
			sb.append('[').append(idx.getColumn()).append(']');
	}
	
	private static void createJavaMethodCode(OperandEnvironment env, BreakingStringBuilder sb, String name, Scalar s, FunctionType parentType, boolean leftChild) {
		if (s instanceof Number) {
			Number n = (Number) s;
			String nstr = Double.toString(((Number)s).toDouble());
			if (n.getNominator().compareTo(BigInteger.ZERO) <= 0 // negative number
					&& parentType != null   // write braces for: --2.0  => -(-2.0) 
					&& (parentType instanceof Negation 
						|| (parentType instanceof Subtraction && !leftChild)))
				sb.append('(').append(nstr).append(')');
			sb.append(nstr);
		} else if (s instanceof Variable) {
			String vname = ((Variable)s).getName();
			if (env != null && env.isReference(vname) && ! vname.startsWith(name)) {
				VariableMatrixReference vmr = env.getReference(vname);
				MatrixIndex mp = selectMatrixIndex(vmr);
				String mname = vmr.getMatrixName();
				sb.append(mname);
				Matrix A = (Matrix) env.get(mname);
				appendMatrixIndexString(sb, A, mp);
			} else
				sb.append(vname);
		} else {
			Function func = (Function) s;
			FunctionType type = func.getType();
			boolean braces = doBraces(parentType, type, leftChild);
			if (braces)
				sb.append('(');
			if (type instanceof Addition) {
				createJavaMethodCode(env, sb, name, func.getInput(0), type, true);
				sb.append('+');
				createJavaMethodCode(env, sb, name, func.getInput(1), type, false);
			} else if (type instanceof Division) {
				createJavaMethodCode(env, sb, name, func.getInput(0), type, true);
				sb.append('/');
				createJavaMethodCode(env, sb, name, func.getInput(1), type, false);
			} else if (type instanceof Inversion) {
				sb.append("1.0/");
				createJavaMethodCode(env, sb, name, func.getInput(0), type, false);
			} else if (type instanceof Multiplication) {
				createJavaMethodCode(env, sb, name, func.getInput(0), type, true);
				sb.append('*');
				createJavaMethodCode(env, sb, name, func.getInput(1), type, false);
			} else if (type instanceof Negation) {
				sb.append('-');
				createJavaMethodCode(env, sb, name, func.getInput(0), type, true); // true because unary minus has associativity right
			} else if (type instanceof Subtraction) {
				createJavaMethodCode(env, sb, name, func.getInput(0), type, true);
				sb.append('-');
				createJavaMethodCode(env, sb, name, func.getInput(1), type, false);
			} else if (type instanceof Sinus) {
				sb.append("Math.sin(");
				createJavaMethodCode(env, sb, name, func.getInput(0), type, true); // TODO associativity ok ?
				sb.append(')');
			} else if (type instanceof Cosinus) {
				sb.append("Math.cos(");
				createJavaMethodCode(env, sb, name, func.getInput(0), type, true); // TODO associativity ok ?
				sb.append(')');
			} else if (type instanceof IfElse) {
				Scalar st = func.getInput(1);
				Scalar sf = func.getInput(2);
				createJavaMethodCode(env, sb, name, func.getInput(0), type, true);
				sb.append('?');
				createJavaMethodCode(env, sb, name, st, type, true);
				sb.append(':');
				createJavaMethodCode(env, sb, name, sf, type, true);
			} else if (type instanceof GreaterTo) {
				createJavaMethodCode(env, sb, name, func.getInput(0), type, true);
				sb.append('>');
				createJavaMethodCode(env, sb, name, func.getInput(1), type, true);
			} else if (type instanceof LowerTo) {
				createJavaMethodCode(env, sb, name, func.getInput(0), type, true);
				sb.append('<');
				createJavaMethodCode(env, sb, name, func.getInput(1), type, true);
			} else if (type instanceof EqualTo) {
				createJavaMethodCode(env, sb, name, func.getInput(0), type, true);
				sb.append("==");
				createJavaMethodCode(env, sb, name, func.getInput(1), type, true);
			} else
				throw new RuntimeException("unknown function type "+type.getName());
			if (braces)
				sb.append(')');
		}
	}

	private static final boolean doBraces(FunctionType parent, FunctionType child, boolean left) {
		if (! REDUCE_BRACES)
			return true;
		if (parent == null)
			return false;
		
		 // prevent to mistake decreasing operator and two negations: --x  =>  -(-x)
		if (parent instanceof Negation && child instanceof Negation)
			return true;
		// prevent f.e.:  a b ~ c * -  =>  a--b*c
		// TODO: this can produce unnecessary braces => specialize
		if (parent instanceof Subtraction && !left)
			return true;
		
		int gp = getGroup(parent);
		int gc = getGroup(child);
		if (gc == gp)
			return !left;
		return gc > gp;
	}

	/* operator precedence:
	 * group 0 : sinus, cosinus
	 * group 1 (associativity R) : unary plus/minus
	 * group 2 (associativity L) : multiplication, division, modulo
	 * group 3 (associativity L) : addition, subtraction
	 * 
	 * a-b+c = (a-b)+c
	 * a+b*c = a+(b*c)
	 * -a*b  = (-a)*b
	 */
	//TODO right values for comparision and if-else ?
	private static final int getGroup(FunctionType type) {
		if (type instanceof Sinus
				|| type instanceof Cosinus
				|| type instanceof IfElse
				|| type instanceof EqualTo
				|| type instanceof LowerTo
				|| type instanceof GreaterTo)
			return 0;
		if (type instanceof Negation)
			return 1;
		if (type instanceof Multiplication
				|| type instanceof Division
				|| type instanceof Inversion)
			return 2;
		if (type instanceof Addition
				|| type instanceof Subtraction)
			return 3;
		if (type instanceof EqualTo
				|| type instanceof LowerTo
				|| type instanceof GreaterTo)
			return 4;
		if (type instanceof IfElse)
			return 5;
		throw new RuntimeException("unknown function type "+type.getName());
	}


	private static List<String> getVariableNames(Operand op) {
		List<String> vars = null;
		if (op instanceof Scalar)
			vars = getVariableNames(null, (Scalar) op);
		else {
			Matrix A = (Matrix) op;
			for (int i = 0; i < A.getHeight(); i++)
				for (int j = 0; j < A.getWidth(); j++) {
					Scalar s = A.getElement(i, j);
					vars = getVariableNames(vars, s);
				}
		}
		Collections.sort(vars);
		return vars;
	}
	
	private static List<String> getVariableNames(List<String> vars, Scalar s) {
		if (vars == null)
			vars = new LinkedList<String>();
		if (s.isAtomic()) {
			if (s instanceof Variable) {
				String name = ((Variable) s).getName();
				if (!vars.contains(name))
					vars.add(name);
			}
		} else {
			Function func = (Function) s;
			for (int i = 0; i < func.getInputCount(); i++)
				getVariableNames(vars, func.getInput(i));
		}
		return vars;
	}
}
