package org.evolvis.math.symbolic.traverse;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.evolvis.math.symbolic.Scalar;

public class SortedScalarList {
	
	/**
	 * sorted, bounded
	 */
	private List<Scalar> sList = new LinkedList<Scalar>();
	private int maxSize;
	private int maxElSize = Integer.MAX_VALUE;
	
	public SortedScalarList(int maxSize) {
		if (maxSize < 1)
			throw new RuntimeException("maximum size must be greater than zero");
		this.maxSize = maxSize;
	}

	public boolean add(Scalar s) {
		int elSize = s.getSize();
		if (elSize < maxElSize) {
			sList.clear();
			sList.add(s);
			maxElSize = elSize;
			return true;
		} else if (elSize == maxElSize) { // scalar has adequate size
			int idx = Collections.binarySearch(sList, s);
		    // Add the non-existent item to the list
			if (idx < 0) { // item is not an element of the list => add
				idx = -idx-1;
		    	sList.add(idx, s);
		    	int size = sList.size();
				if (size > maxSize) {
					int idxr;
					do
						idxr = random.nextInt(size);
					while (idxr == idx);
					sList.remove(idxr);
				}
				return true;
			}
		}
		return false;
	}

	public Scalar get(int idx) {
		return sList.get(idx);
	}

	private static final Random random = new Random();
	
	/**
	 * @return
	 */
	//TODO do no equal distribution: give elements with lower index higher probability
	public Scalar getRandom() {
		int size = sList.size();
		if (size <= 0)
			throw new RuntimeException("no scalars added");
		return sList.get(random.nextInt(size));
	}
	
	public int getSize() {
		return sList.size();
	}
}
