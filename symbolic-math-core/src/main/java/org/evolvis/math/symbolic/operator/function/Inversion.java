package org.evolvis.math.symbolic.operator.function;

import org.evolvis.math.symbolic.Function;
import org.evolvis.math.symbolic.FunctionType;
import org.evolvis.math.symbolic.Matrix;
import org.evolvis.math.symbolic.MatrixOperations;
import org.evolvis.math.symbolic.Number;
import org.evolvis.math.symbolic.Operand;
import org.evolvis.math.symbolic.Scalar;

public class Inversion extends FunctionType {

	private static final String ID = "^-1";
	
	private static FunctionType instance;

	public Inversion() {
		instance = this;
	}
	
	@Override
	public String getName() {
		return ID;
	}

	@Override
	public int getInputCount() {
		return 1;
	}
	
	@Override
	protected Operand apply(Operand... input) {
		if (input[0] instanceof Scalar)
			return apply((Scalar) input[0]);
		return apply((Matrix) input[0]);
	}

	public static final Matrix apply(Matrix matrix) {
		return MatrixOperations.invert(matrix);
	}

	public static final Scalar apply(Scalar node) {
		if (node instanceof Number)
			return ((Number) node).invert();
		return new Function(instance, node);
	}

	@Override
	public Scalar getDerivate(String name, Scalar[] inputs) {
		// (x ^-1) --> (x) ~ x x * /
		Scalar x = inputs[0];
		Scalar xd = x.getDerivate(name);
		return Division.apply(
				Negation.apply(xd),
				Multiplication.apply(x, x));
	}
}
